/**
 * Created by Matt on 3/20/2021.
 */

import { LightningElement } from 'lwc';

export default class ActivateOrderRegisterSysTailPrompt extends LightningElement {
    handleYes(){
        this.dispatchEvent(new CustomEvent('yes'));
    }

    handleNo(){
       this.dispatchEvent(new CustomEvent('no'));
    }

}