/**
 * Created by Matt on 3/24/2021.
 */

import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import activateRecords from '@salesforce/apex/ActivateOrderLWC_Controller.activateRecords';
import checkActivated from '@salesforce/apex/ActivateOrderLWC_Controller.checkActivated';

export default class ActivateOrderActivateRecords extends LightningElement {
    @api orderid;
    isError;
    errorMessage;
    buttonDisabled = true;
    isLoading;

    connectedCallback(){
        this.isLoading = true;
        checkActivated({
            orderId : this.orderid
        })
        .then(result => {
            this.isLoading = false;
            if (result){
                this.isError = true;
                this.errorMessage = 'Order is already activated, no further action is required at this time.';
                this.buttonDisabled = true;
            } else {
                this.buttonDisabled = false;
            }
        })
    }

     showToast(titleInput, msgInput, varInput){
        const event = new ShowToastEvent({
            title: titleInput,
            message: msgInput,
            variant: varInput
        });
        this.dispatchEvent(event);
     }

     closeQuickAction() {
         const closeQA = new CustomEvent('close');
         this.dispatchEvent(closeQA);
     }


     handleActivate(){
        this.isLoading = true;
        activateRecords({
            orderId : this.orderid
        })
        .then(result => {
            this.isLoading = false;
            if (result.isSuccess){
                this.showToast('','Order and Contract successfully activated!', 'success');
                this.closeQuickAction();
            } else {
                console.log('ERROR: ', result.errorMsg);
                this.showToast('ERROR:', result.errorMsg, 'error');
            }
        })
        .catch(error => {
            this.isLoading = false;
            this.showToast('ERROR:', error.body.message, 'error');
            console.log('ERROR: ', error.body.message);
        });
    }
}