/**
 * Created by Matt on 3/19/2021.
 */

import { LightningElement, api, track } from 'lwc';

export default class ActivateOrderSelectProdSys extends LightningElement {
    @api prodsystems;
    @api checkboxarray = [];
    selectedCheckboxes = [];
    buttonDisabled = true;
    @track value = [];
    @api missingSystems = []; // Holds the user-selections that DON'T have a corresponding Production System record
    @api validSelections = []; // Holds the user-selections that have a corresponding Production System record
    closeQuickAction() {
        const closeQA = new CustomEvent('close');
        this.dispatchEvent(closeQA);
    }

    handleBack(){
        this.dispatchEvent(new CustomEvent('goback'));
    }

    handleChange(event){
        this.value = event.detail.value;
        if (this.value.length > 0){
            this.buttonDisabled = false;
        } else {
            this.buttonDisabled = true;
        }
    }

    handleNext(){
        this.missingSystems = [];
        this.selectedCheckboxes = this.value.join(',');
        this.selectedCheckboxes = this.selectedCheckboxes.split(',');
        this.selectedCheckboxes.forEach(item => {
            if (item.includes("MISSING ")){
                this.missingSystems.push(item.replace("MISSING",""));
            }
        });
        console.log('selected checkBoxes: ', this.selectedCheckboxes);
        if (this.missingSystems.length > 0){
            this.dispatchEvent(new CustomEvent('missingselections', {
                detail: this.missingSystems
            }));
        } else {
            this.dispatchEvent(new CustomEvent('validselectionsmade', {
                detail: this.selectedCheckboxes
              }));
        }
    }
}