/**
 * Created by Matt on 3/30/2021.
 */

import { LightningElement } from 'lwc';

export default class ActivateOrderNoContractFound extends LightningElement {


    closeQuickAction() {
         const closeQA = new CustomEvent('close');
         this.dispatchEvent(closeQA);
    }

}