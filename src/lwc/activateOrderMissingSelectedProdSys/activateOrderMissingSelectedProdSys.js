/**
 * Created by Matt on 3/22/2021.
 */

import { LightningElement, api } from 'lwc';
import ERROR_MSG from '@salesforce/label/c.activateOrder_NoSelectedProdSysError';

export default class ActivateOrderMissingSelectedProdSys extends LightningElement {
    noProdSysErrorMsg = ERROR_MSG;
    @api missingsystems;

    handleBack(){
            this.dispatchEvent(new CustomEvent('goback'));
        }

        handleClose(){
            this.dispatchEvent(new CustomEvent('close'));
        }
}