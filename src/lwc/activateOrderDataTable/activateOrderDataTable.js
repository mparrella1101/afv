/**
 * Created by Matt on 3/22/2021.
 */

import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSpecificOrderLineData from '@salesforce/apex/ActivateOrderLWC_Controller.getSpecificOrderLineData';
import buildSystemTailLists from '@salesforce/apex/ActivateOrderLWC_Controller.buildSystemTailLists';
import updateOrderLineItems from '@salesforce/apex/ActivateOrderLWC_Controller.updateOrderLineItems';

const columns = [
    { label: 'Product', fieldName: 'productName', editable: false },
    { label: 'Aircraft', fieldName: 'aircraft', editable: false },
    { label: 'System Tail', fieldName: 'availableTails', editable: true }
];


export default class ActivateOrderDataTable extends LightningElement {
 @api selectedsystems = [];
 @api accountid;
 @api orderid;
 systemTails;
 data = [];
 columns = columns;
 rowOffset = 0;
 noRecordsFound = false;
 recordUpdateMap = new Map();
 isLoading = false;
 duplicatesFound = false;
 prodSystems = [];
 noConnectedCallback = false;
 tailId = '';
 sortedDirection = 'asc';
 sortedColumn;

 connectedCallback(){
    this.isLoading = true;
    this.selectedsystems.forEach(item => {
         this.prodSystems.push(item);
     });
     getSpecificOrderLineData({
         orderId: this.orderid,
         accountId: this.accountid,
         prodSystems: this.prodSystems
     })
     .then(result => {
         this.data = result;
         if (result.length === 0){
             this.noRecordsFound = true;
         } else {
             this.noRecordsFound = false;
         }
         this.isLoading = false;
         console.log(this.data);
     })
     .catch(error => {
         this.isLoading = false;
         this.showToast('ERROR:', error.body.message, 'error');
         console.log('ERROR: ', error.body.message);
     })

 }

 handleChange(event){
     // This updates the map by associating the Order Line Record Id -> new System Tail Record Id
     this.recordUpdateMap.set(event.target.dataset.id, event.target.value);
     //Clone this.data
     var dataClone = this.data;
     //Loop through elements of dataClone to find the matching selected row Id and assign the selected tail's value to that row
     for(var i=0; i < dataClone.length; i++){
         if(dataClone[i].id == event.target.dataset.id){
             dataClone[i].tailId = event.target.value;

         }
         else {
             if(!dataClone[i].tailId){
             dataClone[i].tailId = undefined;
        }
    }
     }
     //Update this.data
     this.data = dataClone;


     this.duplicatesFound = false;

     console.log(this.data);
     console.log(this.recordUpdateMap);

 }

 closeQuickAction() {
         const closeQA = new CustomEvent('close');
         this.dispatchEvent(closeQA);
 }

 showToast(titleInput, msgInput, varInput){
    const event = new ShowToastEvent({
        title: titleInput,
        message: msgInput,
        variant: varInput
    });
    this.dispatchEvent(event);
 }

 handleSave(){
     this.isLoading = true;
     this.duplicatesFound = false;
     let recordData = []; // Holds all new JS objects that are being sent to Apex
     // Iterate through map and create JS objects to send to Apex
     for (let [key,value] of this.recordUpdateMap) {
         let newObj = new Object();
         newObj.recordId = key;
         newObj.tailId = value;
         recordData.push(newObj);
     }
//     // check for duplicate tailIds
//     let tailIdArray = recordData.map(function(item){ return item.tailId });
//     this.duplicatesFound = tailIdArray.some(function(item, idx){
//         return tailIdArray.indexOf(item) !== idx;
//     });
//
//     if (this.duplicatesFound){
//         // if we found dupes, we want to stop the loading spinner to display the error message, else continue on with processing
//         this.isLoading = false;
//     }


     // Pass the map to apex controller to perform updates
//    if (!this.duplicatesFound){
         updateOrderLineItems({
             inputVal: JSON.stringify(recordData)
         })
         .then(result => {
             this.isLoading = false;
             if (result.isSuccess){
               this.showToast('','Order Items successfully updated!', 'success');
               this.dispatchEvent(new CustomEvent('updated')); // Should redirect user to 'activate order' lwc
             } else {
                 this.showToast('', 'Error: ' + resp.errorMsg, 'error');
             }
         })
        .catch(error => {
            this.isLoading = false;
            this.showToast('ERROR:', resp.errorMsg, 'error');
        });
    //}
 }

sortRows(e){
    var comboBoxes1 = this.template.querySelectorAll("lightning-combobox");
    console.log("Row IDs Before");

    for(var i = 0; i < comboBoxes1.length; i++){

        console.log(comboBoxes1[i].dataset.id);
    }
    if(this.sortedColumn === e.currentTarget.dataset.id){
        this.sortedDirection = this.sortedDirection === 'asc' ? 'desc' : 'asc';
    }else{
        this.sortedDirection = 'asc';
    }
    var reverse = this.sortedDirection === 'asc' ? 1 : -1;
    let table = JSON.parse(JSON.stringify(this.data));
    table.sort((a,b) => {return a[e.currentTarget.dataset.id] > b[e.currentTarget.dataset.id] ? 1 * reverse : -1 * reverse});
    this.sortedColumn = e.currentTarget.dataset.id;
    this.data = table;
    this.assignTailValues();

}
assignTailValues(){
    var comboBoxes = this.template.querySelectorAll("lightning-combobox");

    for(var i = 0; i < comboBoxes.length; i++){
        for(var j = 0; j < this.data.length; j++){
            if(comboBoxes[i].dataset.id == this.data[j].id){
                //console.log("comboBoxes[i].dataset.id" + comboBoxes[i].dataset.id);console.log("this.data[j].id");console.log(this.data[j].id);
                for(var k = 0; k < this.data[j].availableTails.length; k++)
                {
                    if(this.data[j].availableTails[k].value == this.data[j].tailId && this.data[j].tailId != '' && this.data[j].availableTails[k].value != ''){
                        comboBoxes[i].value = this.data[j].availableTails[k].value;
                        //console.log(i + " comboBoxes[i].value: ");console.log(comboBoxes[i].value);
                        //console.log(i + " this.data[j].availableTails[k].value: "); console.log(this.data[j].availableTails[k].value);
                    }
                }
            }
        }
    }
    console.log("Row IDs After")
    for(var i = 0; i < 3; i++){
       console.log(comboBoxes[i].dataset.id);

    }

}


}