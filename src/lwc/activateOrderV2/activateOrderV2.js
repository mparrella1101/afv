import { LightningElement, api, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
/* Apex Methods Import: START */
import markOppAsWon from '@salesforce/apex/ActivateOrderController_ver2.markOppAsWon';
import processQuoteLines from '@salesforce/apex/ActivateOrderController_ver2.processQuoteLines';
import processAmendmentSubs from '@salesforce/apex/ActivateOrderController_ver2.processAmendmentSubs';
import activateContractAcceptQuote from '@salesforce/apex/ActivateOrderController_ver2.activateContractAcceptQuote';
/* Apex Methods Import: STOP */

import QUOTE_TYPE_FIELD from '@salesforce/schema/SBQQ__Quote__c.SBQQ__Type__c';

const fields = [QUOTE_TYPE_FIELD];


export default class ActivateOrderV2 extends LightningElement {
    @api recordId;

    /* Quote Type vars: START */
    isQuote = false;
    isAmendment = false;
    isRenewal = false;
    quoteType; // holds the quote type value
    registrationClass = 'registration';
    registration = 'Registration';
    amendResult; // Stores data returned from apex
    /* Quote Type vars: STOP */

    /* Loading screen vars: START */
    isLoading = true; // Controls the showing/hiding of the Lightning Spinner
    loadingText = 'Initializing. Please wait...'; // Displayed on screen when 'showLoadingScreen' = TRUE and 'isLoading' = TRUE
    /* Loading screen vars: STOP */

    /* UI Behavior Vars: START */
    showHomeScreen = false; // When TRUE, shows the main screen asking the user if they'd like to register system tails
    showLoadingScreen = true; // When TRUE, shows lightning spinner + custom loading message
    showDataTable = false; // When TRUE, we have QuoteLine data and should display the table for the user to assign Sys Tails to Quote Lines
    showAmendDataTable = false; // When TRUE, we have QuoteLine data and should display the table for the user
    isRemoving = false; // When TRUE, we have tails to deregister and thus we need to bring the user to that screen next
    hasAdditions = false; // When TRUE, there have been added subscriptions [Amendment use only]
    showNoTailsFound = false; // When TRUE, displays message indicating there were no subscriptions found to register tails for
    /* UI Behavior Vars: STOP */

    /* Data + Data-Helper vars: START */
    quoteLineData; // Stores QuoteLine data returned from Apex 'processQuoteLines' call
    allQuoteLineProdSystems = []; // Stores all the Production_System__c records present within the QuoteLines (used to promp the user if they'd like to register more tails)
    
    /* Data + Data-Helper vars: STOP */

    @wire(getRecord, { recordId: '$recordId', fields })
    wiredQuote({ data, error }) {
        if (data) {
            if (data.fields.SBQQ__Type__c.value) {
                switch (data.fields.SBQQ__Type__c.value) {
                    case 'Quote':
                        this.quoteType = 'Quote';
                        this.showHomeScreen = true; // Show the option for registering sys tails
                        this.setIsLoading(false);
                        break;
                    case 'Amendment':
                        this.quoteType = 'Amendment';
                        this.loadingText = 'Gathering Amendment data. Please wait...';
                        this.processAmendment();
                        break;
                    case 'Renewal':
                        this.isRenewal = true;
                        this.quoteType = 'Renewal';
                        this.loadingText = 'Processing Renewal Data. Please wait...';
                        this.processAmendment();
                        break;
                    default:
                        console.log('ERROR: No Quote Type found.');
                        break;
                }
            }
        }
    }

    /* Method to call the 'processAmendmentSubs' apex method in order to check the subs between new and old quote */
    processAmendment(){
        processAmendmentSubs({
            quoteId : this.recordId
        })
        .then(result => {
            if (result.isSuccess){
                this.amendResult = JSON.parse(JSON.stringify(result));
                console.log('processAmendment() result: ', this.amendResult);
                if (this.amendResult.amendedData['removed']){
                    if (this.amendResult.amendedData['removed'].length > 0){
                        this.isRemoving = true;
                        this.registrationClass = 'deregistration';
                        this.registration = 'DE-Registration';
                        this.quoteLineData = result.amendedData['removed'];
                        this.setIsLoading(false);
                        this.showDataTable = true;
                    }
                }
                else if (this.amendResult.amendedData['added']){
                    if (this.amendResult.amendedData['added'].length > 0) {
                        this.processAdditionalSubs();
                    }
                }
                else {
                    this.showHomeScreen = true;
                    this.setIsLoading(false);
                }
            } else {
                console.log('ERROR Processing Quote: ', result.errorMsg);
            }
        })
        .catch(error => {
            console.log('processAmendment() ERROR: ', error);
        });
    }

    /* Event Handler for when the user selects 'Yes' on the Register System Tail screen. It will determine which data to gather for
    the following screen (QuoteLine or Subscription) based on the Quote Type */
    handleYesEvent(){
        // Determine which method to call based on Quote Type (QuoteLine data vs. Subscription data)
        switch (this.quoteType) {
            case 'Quote':
                this.registerTails();
                break;
            case 'Amendment':
                let tempResult = this.amendResult.amendedData;
                // Only execute 'processAdditionalSubs' function if we have added subs
                if (tempResult['added'] && tempResult['added'].length > 0){
                    this.processAdditionalSubs();
                } else {
                    this.setIsLoading(false);
                    this.showHomeScreen = false;
                    this.showNoTailsFound = true;
                }
                break;
            case 'Renewal':
                this.registerTails();
                break;
            default:
                console.log('Alas! A unicorn!');
                break;
        }
    }

    /* Method to load the data table with any added subscriptions [Amendment only] */
    processAdditionalSubs() {
        this.setIsLoading(true);
        this.loadingText = 'Checking for any added subscriptions. Please wait...';
        processAmendmentSubs({
            quoteId : this.recordId
        })
        .then(result => {
            if (result.isSuccess){
                if (this.amendResult.amendedData['added'].length > 0){
                    this.registrationClass = 'registration';
                    this.registration = 'Registration';
                    this.quoteLineData = this.amendResult.amendedData['added'];
                    this.setIsLoading(false);
                    this.isRemoving = false;
                    this.showHomeScreen = false;
                    this.showDataTable = true;
                }
            }
        })
        .catch(error => {
            console.log('ERROR: ', error);
        });

    }

    /* Method to call the 'processQuoteLines' apex method in order to retrieve related QuoteLine data */
    registerTails() {
            this.showHomeScreen = false;
            this.setIsLoading(true);
            this.loadingText = 'Analyzing Quote. Please wait...';
            processQuoteLines({
                quoteId : this.recordId
            })
            .then(result => {
                console.log('RegisterTails() result: ', result);
                if (result.isSuccess){
                    this.quoteLineData = result.quoteLineData;
                    if (this.quoteLineData){
                        if (this.quoteLineData.length > 0) {
                            console.log('quoteline data present');
                            this.setIsLoading(false);
                            this.getQuoteLineProductFamilies(this.quoteLineData);
                            this.showDataTable = true;
                        }
                    } else {
                        this.setIsLoading(false);
                        this.showNoTailsFound = true;
                    }
                } else {
                    this.setIsLoading(false);
                    this.showToast('',result.errorMsg, 'error');
                    this.closeQuickAction();
                }
            })
            .catch(error => {
                console.log(error);
            });

            // Call the correct method to get the added subs

    }

    /* Method to build list of all Product Families present within the Quote Lines */
    getQuoteLineProductFamilies(quoteLineInput){
        quoteLineInput.forEach(quoteLine =>  {
                if (this.allQuoteLineProdSystems.indexOf(quoteLine.productFamily) === -1){
                    this.allQuoteLineProdSystems.push(quoteLine.productFamily);
                }
        });
    }

    /* Method to check the 'Opportunity won' checkbox on the related opportunity (if user selects 'no' when prompted to register sys tails) */
    activateOrder() {
        this.loadingText = 'Closing the related Opportunity. Please Wait...';
        this.setIsLoading(true);
        // Attempt to check the 'isWon'
        markOppAsWon({
            quoteId : this.recordId
        })
        .then(result => {
           if (result.isSuccess) {
               this.showToast('','Opportunity successfully closed!', 'success');
               this.closeQuickAction();
           }
        })
        .catch(error => {
           console.log(error);
        });
    }

    /* Helper method to close the Quick Action modal */
    closeQuickAction() {
            this.dispatchEvent(new CloseActionScreenEvent());
    }

    /* Helper method to display toast messages */
    showToast(inputTitle, inputMsg, inputVariant) {
            this.dispatchEvent(new ShowToastEvent({
                title: inputTitle,
                message: inputMsg,
                variant: inputVariant
            }));
        }

    /* Helper method to control the 'isLoading' param */
    setIsLoading(input) {
        this.isLoading = input;
        if (input === true){
            this.showLoadingScreen = true;
        } else {
            this.showLoadingScreen = false;
        }
    }

    /* Event handler for setting the isLoading parameter on the parent LWC */
    handleSetLoading(event){
        this.loadingText = event.detail;
        this.setIsLoading(true);
    }

    /* Event handler for unsetting the isLoading parameter on the parent LWC */
    handleUnsetLoading() {
        this.setIsLoading(false);
    }

    /* Event handler for returning back to the Register System Tails screen */
    handleGoHome(event) {
        this.showDataTable = false;
        this.showHomeScreen = true;
    }

    handleShowToast(event){
        let payload = JSON.parse(JSON.stringify(event.detail));
        this.showToast(payload.title, payload.message, payload.variant);
    }

    /* Event handler for when there are no added subs and user clicks 'OK' to activate contract + accept quote */
    handleActivateEvent(){
        this.loadingText = 'Activating Contract and accepting Quote. Please wait...';
        this.setIsLoading(true);
        activateContractAcceptQuote({
            quoteId : this.recordId
        })
        .then(result => {
            this.setIsLoading(false);
            if (result.isSuccess){
                this.showToast('','Quote as been accepted and Contract activated!', 'success');
                this.dispatchEvent(new CloseActionScreenEvent());
            } else {
                this.showToast('', result.errorMsg, 'error');
                this.dispatchEvent(new CloseActionScreenEvent());
            }
        })
        .catch(error => {
            this.setIsLoading(false);
            console.log('***Error Activating Contract: ', error);
            this.showToast('', 'Exception caught. Please check browser console for details (F12).', 'error');
            this.dispatchEvent(new CloseActionScreenEvent());
        })
    }

    handleCancel(){
        this.dispatchEvent(new CloseActionScreenEvent());
    }

}