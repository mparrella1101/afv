/**
 * Created by Matt on 3/7/2022.
 */

import { LightningElement, api } from 'lwc';
import activateContractAcceptQuote from '@salesforce/apex/ActivateOrderController_ver2.activateContractAcceptQuote';
import { CloseActionScreenEvent } from 'lightning/actions';

export default class NoTailsScreenv2 extends LightningElement {

    @api recordId;

    handleOK(){
        this.dispatchEvent(new CustomEvent('activatecontract'));
    }
    handleCancel(){
        this.dispatchEvent(new CustomEvent('cancel'));
    }
}