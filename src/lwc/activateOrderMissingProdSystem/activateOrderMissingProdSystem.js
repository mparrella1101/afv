/**
 * Created by Matt on 3/20/2021.
 */

import { LightningElement, api } from 'lwc';
import MISSING_PROD_SYS_ERROR from '@salesforce/label/c.activateOrder_MissingProdSysError';

export default class ActivateOrderMissingProdSystem extends LightningElement {
    @api missingsystems; // Holds actual Production_System__c.System__c values that are not related to the Account
    errorMsg = MISSING_PROD_SYS_ERROR;

    closeQuickAction() {
                 const closeQA = new CustomEvent('close');
                 this.dispatchEvent(closeQA);
        }
}