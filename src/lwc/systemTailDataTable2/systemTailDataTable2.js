/**
 * Created by Matt on 2/25/2022.
 */

import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import createAircraftSubscriptions from '@salesforce/apex/ActivateOrderController_ver2.createAircraftSubscriptions';
import deregisterSysTails from '@salesforce/apex/ActivateOrderController_ver2.deregisterSysTails';

const columns = [
    { label: 'Product', fieldName: 'productName', editable: false },
    { label: 'Aircraft', fieldName: 'aircraft', editable: false },
    { label: 'System Tail', fieldName: 'availableTails', editable: true }
];


export default class SystemTailDataTable2 extends LightningElement {
     systemTails;
     @api recordId;
     @api quoteLineData; // Passed-in QuoteLine data returned from the Apex method (processQuoteLines())
     @api prodSystems; // Passed-in list of production systems present
     columns = columns; // Data table columns
     rowOffset = 0;
     isLoading = false;
     tailId = '';
     sortedDirection = 'asc';
     sortedColumn;
     recordUpdateMap = new Map(); //
     showSaveConfirmation = false; // Var to control confirmation pop-up when user clicks 'Save' button
     hasAPGTails = false; // When TRUE, available APG System Tails were found that can be registered
     hasRRTails = false; // When TRUE, available RocketRoute System Tails were found that can be registered
     hasSATails = false; // When TRUE, available Seattle Avionics System Tails were found that can be registered
     apgTails = []; // Holds all QuoteLines that have an APG Product Family
     rrTails = [];  // Holds all QuoteLines that have a RocketRoute Product Family
     saTails = []; // Holds all QuoteLines that have a Seattle Avionics Product Family
     quoteLineMap = new Map(); // Array to hold all quoteline data (QuoteLineId -> QuoteLineWrapper)
     selectedQuoteLine; // Holds the value of which quoteline was selected
     tempTail; // Temporarily holds the previous combobox value (should we need to revert due to already selected)

     /* Already Selected modal vars: START */
     selectedProduct;
     selectedAircraft;
     showAlreadyRegistered = false; // When TRUE, will display pop-up warning indicating the System Tail has already been selected
     @api isDeregistering = false; // When TRUE, we are displaying lines that need system tails removed
     /* Already Selected modal vars: STOP */

     @api registrationClass;
     @api registration;
     @api isRemoving;

     picklistsMap = new Map();

     /* Will execute after method has rendered and all variables have been assigned/initialized. Sorts quote lines into product family groups */
     connectedCallback() {
             this.quoteLineData.forEach(record => {
                 let tempRecord = JSON.parse(JSON.stringify(record));
                 this.quoteLineMap.set(tempRecord.quoteLineId, tempRecord);
                 if (tempRecord.isRegistered && tempRecord.selectedTail){
                      if (tempRecord.selectedTail.value !== null){
                        this.recordUpdateMap.set(tempRecord.listTag + '_' + tempRecord.selectedTail.value, tempRecord);
                      }
                 }
                 if (!tempRecord.isRegistered){
                    this.picklistsMap.set(tempRecord.listTag, tempRecord.sysTailWrappers);
                 } else {
                     this.picklistsMap.set(tempRecord.listTag, null);
                 }

                 switch (record.productFamily) {
                      case 'APG':
                          this.apgTails.push(tempRecord);
                          break;
                      case 'RocketRoute':
                          this.rrTails.push(tempRecord);
                          break;
                      case 'Seattle Avionics':
                          this.saTails.push(tempRecord);
                          break;
                      default:
                          console.log('Alas! A unicorn!');
                          break;
                 }
                 tempRecord.sysTailWrappers = this.picklistsMap.get(tempRecord.listTag);
              });


         this.hasAPGTails = this.apgTails.length > 0 ? true : false;
         this.hasRRTails = this.rrTails.length > 0 ? true : false;
         this.hasSATails = this.saTails.length > 0 ? true : false;

         this.sortInputs(this.quoteLineData);
     }


     handleChange(event){
         let tailClone = [];
         let dataClone = [];

         // Copy all lists into one list for processing
         dataClone = [...this.apgTails, ...this.rrTails, ... this.saTails];
         dataClone = JSON.parse(JSON.stringify(dataClone));

         // Iterate over data and remove the selected value from the picklist
         dataClone.forEach(item => {
            if (item.quoteLineId === event.target.dataset.id){
                item.isRegistered = true;
                item.canRemove = true;
                let tailName = event.target.options.find(opt => opt.value === event.detail.value).label;
                item.selectedTail = {
                    value: event.target.value,
                    label: tailName
                };
            }
            // Clear the value from the picklist
            let picklistClone = [];
            picklistClone = JSON.parse(JSON.stringify(this.picklistsMap.get(item.listTag)));
            if (picklistClone){
                tailClone = picklistClone.filter(tail => tail.value !== event.detail.value);
            } else {
                tailClone = [];
            }
            this.picklistsMap.set(item.listTag, tailClone);
            item.sysTailWrappers = this.picklistsMap.get(item.listTag);

         });
         this.sortInputs(dataClone);
     }

     handleIconClick(event){
        let tailClone = [];
        let dataClone = [];

        // Copy all lists into one list for processing
        dataClone = [...this.apgTails, ...this.rrTails, ... this.saTails];
        dataClone = JSON.parse(JSON.stringify(dataClone));

        dataClone.forEach(item => {
           if (item.quoteLineId === event.target.dataset.id){
               item.isRegistered = false;
               tailClone = this.picklistsMap.get(item.listTag);
               tailClone.push(item.selectedTail);
               item.canRemove = false;
               item.selectedTail = {label: '', value: null};
               this.picklistsMap.set(item.listTag, tailClone);
           }
           item.sysTailWrappers = JSON.parse(JSON.stringify(this.picklistsMap.get(item.listTag)));
        });
        this.sortInputs(dataClone);
     }

     closeQuickAction() {
             const closeQA = new CustomEvent('close');
             this.dispatchEvent(closeQA);
     }

     showToast(titleInput, msgInput, varInput){
        const event = new ShowToastEvent({
            title: titleInput,
            message: msgInput,
            variant: varInput
        });
        this.dispatchEvent(event);
     }

     /* Method to display confirmation dialogue when user hits 'Save' button */
     confirmSave() {
        this.showSaveConfirmation = true;
     }

     /* Executed when the user clicks 'Cancel' on the Save confirmation modal */
     closeSaveConfirm() {
         this.showSaveConfirmation = false;
     }

    /* Executed when the user clicks 'Save' on the Save confirmation modal */
    handleSave(){
        // If we're not deregistering, then call the apex method that creates aircraft reg
        if (!this.isRemoving){
           this.dispatchEvent(new CustomEvent('setloading', { detail: 'Registering System Tails. Please wait...' }));
           let recordData = []; // Holds all new JS objects that are being sent to Apex

            // Iterate through data lines that are registered and create JS objects to send to Apex
            let dataClone = [];
            dataClone = [...this.apgTails, ...this.rrTails, ...this.saTails];
            dataClone.forEach(item => {
                if (item.isRegistered){
                    let newObj = new Object();
                    newObj.sysTailId = item.selectedTail.value;
                    newObj.productId = item.productId;
                    newObj.productFamily = item.productFamily;
                    newObj.aircraftId = item.aircraftId;
                    recordData.push(newObj);
                }
            });

            // Call the apex method to create Aircraft Registration records
            createAircraftSubscriptions({
               quoteLineSysTailData : JSON.stringify(recordData),
               quoteId: this.recordId
            })
            .then(result => {
               if (result.isSuccess){
                   this.fireShowToastEvent('', 'System Tail(s) successfully registered!', 'success');
                   this.dispatchEvent(new CustomEvent('unsetloading'));
                   this.closeQuickAction();
               } else {
                   this.fireShowToastEvent('', result.errorMsg, 'error');
                   this.dispatchEvent(new CustomEvent('unsetloading'));
               }
            })
            .catch(error => {
                this.dispatchEvent(new CustomEvent('unsetloading'));
               console.log(error);
            });
        } else {
            /* System Tail De-registration: START */
            let recordData = [];
                this.dispatchEvent(new CustomEvent('setloading', { detail: 'De-registering System Tails. Please wait...'}));
                let dataClone = [];
                dataClone = [...this.apgTails, ...this.rrTails, ...this.saTails];
                dataClone.forEach(item => {
                    console.log('item: ', item);
                    if (item.selectedTail.value){
                        let newObj = new Object();
                        newObj.sysTailId = item.selectedTail.value;
                        newObj.productId = item.productId;
                        newObj.productFamily = item.productFamily;
                        newObj.aircraftId = item.aircraftId;
                        recordData.push(newObj);
                    }
                });
                console.log('recordData: ', recordData);

                // Call apex method to delete Aircraft Registration records
                deregisterSysTails({
                    quoteLineSysTailData : JSON.stringify(recordData),
                    quoteId : this.recordId
                })
                .then(result => {
                    console.log('De-register result: ', result);
                    if (result.isSuccess){
                        this.fireShowToastEvent('', 'Successfully de-registered System Tails!', 'success');
                        this.dispatchEvent(new CustomEvent('unsetloading'));
                        this.fireGoHomeEvent(); // After successful deregistration, prompt user if they'd like to register added tails
                    }
                })
                .catch(error => {

                });
        }

    }

    /* Function to fire event that will cause the Parent LWC to load the 'Home' screen (register sys tail prompt) */
    fireGoHomeEvent(){
        this.dispatchEvent(new CustomEvent('gohome'));
    }

    sortRows(e){
        var comboBoxes1 = this.template.querySelectorAll("lightning-combobox");
        if(this.sortedColumn === e.currentTarget.dataset.id){
            this.sortedDirection = this.sortedDirection === 'asc' ? 'desc' : 'asc';
        }else{
            this.sortedDirection = 'asc';
        }
        var reverse = this.sortedDirection === 'asc' ? 1 : -1;
        let table = JSON.parse(JSON.stringify(this.quoteLineData));
        table.sort((a,b) => {return a[e.currentTarget.dataset.id] > b[e.currentTarget.dataset.id] ? 1 * reverse : -1 * reverse});
        this.sortedColumn = e.currentTarget.dataset.id;
        this.quoteLineData = table;
        this.assignTailValues();
    }

    assignTailValues(){
        var comboBoxes = this.template.querySelectorAll("lightning-combobox");

        for(var i = 0; i < comboBoxes.length; i++){
            for(var j = 0; j < this.quoteLineData.length; j++){
                if(comboBoxes[i].dataset.id == this.quoteLineData[j].quoteLineId){
                    for(var k = 0; k < this.quoteLineData[j].systemTails.length; k++)
                    {
                        if(this.quoteLineData[j].availableTails[k].value == this.quoteLineData[j].registeredSysTailId && this.quoteLineData[j].registeredSysTailId != '' && this.quoteLineData[j].systemTails[k].value != ''){
                            comboBoxes[i].value = this.quoteLineData[j].systemTails[k].value;
                        }
                    }
                }
            }
        }
    }

/* This method is executed when the user clicks 'OK' on the modal displaying that a System Tail has already been selected for that Quote Line configuration */
    handleWarningConfirm(){
        let combobox =this.template.querySelector("[data-id=" + this.selectedQuoteLine.quoteLineId + "]");
        combobox.value = this.tempTail;

        this.showAlreadyRegistered = false;
    }

    /* Helper method to sort all the Quote Line data into 3 production-system-specific lists */
    sortInputs(list){
        let parsedList = JSON.parse(JSON.stringify(list));
        let apgTemp = [];
        let rrTemp = [];
        let saTemp = [];
        if (parsedList !== null){
            parsedList.forEach(item => {
                switch (item.productFamily) {
                    case 'APG':
                        apgTemp.push(item);
                        break;
                    case 'RocketRoute':
                        rrTemp.push(item);
                        break;
                    case 'Seattle Avionics':
                        saTemp.push(item);
                        break;
                    default:
                        console.log('Alas! A unicorn!!');
                }
            });

            // Reassign to tracked variables to trigger re-render
            this.apgTails = apgTemp;
            this.rrTails = rrTemp;
            this.saTails = saTemp;
        }
    }

    /* Helper method to store the current value in the System Tail combobox, before it is changed. This is used when a tail is already selected for a matching Quote Line and we need to restore the original value */
    handleFocus(event) {
        let dataClone = [];
        // Copy all lists into one list for processing
        dataClone = [...this.apgTails, ...this.rrTails, ... this.saTails];
        dataClone = JSON.parse(JSON.stringify(dataClone));
        dataClone.forEach(item => {
            if (item.quoteLineId === event.target.dataset.id){
                item.sysTailWrappers = this.picklistsMap.get(item.listTag);
            }
        });
        this.sortInputs(dataClone);
    }

    fireShowToastEvent(inputTitle, inputMsg, inputVar){
            let payload = {
                title : inputTitle,
                message : inputMsg,
                variant : inputVar
            };
            const toastEvt = new CustomEvent('showtoast', { detail : payload });
            this.dispatchEvent(toastEvt);
    }
}