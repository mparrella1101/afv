/**
 * Created by Matt on 2/25/2022.
 */

import { LightningElement, api } from 'lwc';
import markOppAsWon from '@salesforce/apex/ActivateOrderController_ver2.markOppAsWon';

export default class RegisterSysTailPrompt2 extends LightningElement {
    @api recordId;
    showNoConfirm = false; // When user clicks 'No', we need to alert them that records will still be updated and/or activated

        /* Method to fire custom event informing the parent LWC to load the datatable for registering system tails.
           This method will also call an Apex method to set the related Opp to 'Closed won', which should trigger
           the creation of SBQQ__Subscription__c records for each SBQQ__QuoteLine__c record */
        handleYes(){
            // Attempt to close the Opp so Contract + Subs can be created as they are needed for after the system tails have been registered
            markOppAsWon({
                quoteId : this.recordId
            });
            this.dispatchEvent(new CustomEvent('yes'));
        }

        handleNo(){
            this.showNoConfirm = true;
        }

        handleRegCancel() {
            this.dispatchEvent(new CustomEvent('abandon'));
        }

        handleActivateConfirm() {
            this.dispatchEvent(new CustomEvent('no')); // Will trigger the contract activation and quote acceptance record updates
        }

        handleModalCancel() {
            this.showNoConfirm = false;
        }

}