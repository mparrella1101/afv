/**
 * Created by Matt on 3/19/2021.
 */

import { LightningElement, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import contractPresent from '@salesforce/apex/ActivateOrderLWC_Controller.contractPresent';
import getProductionSystemRecords from '@salesforce/apex/ActivateOrderLWC_Controller.getProductionSystemRecords';
import getOrderLineData from '@salesforce/apex/ActivateOrderLWC_Controller.getOrderLineData';

const FIELDS = [
    'Order.Id',
    'Order.AccountId',
    'Order.SBQQ__Quote__r.SBQQ__Type__c'
];

export default class ActivateOrder extends LightningElement {

@api recordId; //Order Record Id
@api userChoice; //The user's choice from the 'Would you like to register system tails' screen
@api choiceSelected;
@api accountId;
orderId;
quoteType;
hasManyProdSystems;
hasOneProdSystem;
hasNoProdSystems;
hasSystemTails;
prodSystems;
showCmp = true;
missingProdSys; // Boolean to determine if we have an issue w/ a missing prod system
missingSystem = []; // Stores the name(s) of the missing prod system
missingSystemName; // displays missing prod system names nicely
isLoading;
@api checkboxArray = [];
orderLineItems;
userSelectionMade = false;
standardQuote;
renewalQuote;
amendmentQuote;
contractPresentValue;
showMissingSelectionError = false;
@api missingSelections = [];
@api validSelections = [];
tailsDeregistered = false;



    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    orderRecord ({ data, error}){
        if (error){
            console.log('ERROR: ', JSON.parse(JSON.stringify(error)));
        } else if (data){
            this.isLoading = true;
            // Check if Contract is present first
            contractPresent({
                orderId : this.recordId
            })
            .then(result => {
                if (result){
                    this.contractPresentValue = true;
                     // Assign values to local variables
                     this.accountId = data.fields.AccountId.value !== null ? data.fields.AccountId.value : null;
                     this.orderId = data.fields.Id.value !== null ? data.fields.Id.value : null;
                     this.quoteType = data.fields.SBQQ__Quote__r.value !== null ? data.fields.SBQQ__Quote__r.value.fields.SBQQ__Type__c.value : 'Quote';
                     switch (this.quoteType){
                         case 'Quote':
                             this.standardQuote = true;
                             this.renewalQuote = false;
                             this.amendmentQuote = false;
                             break;
                         case 'Renewal':
                             this.renewalQuote = true;
                             this.standardQuote = false;
                             this.amendmentQuote = false;
                             break;
                         case 'Amendment':
                             this.amendmentQuote = true;
                             this.standardQuote = false;
                             this.renewalQuote = false;
                             break;
                         default:
                            console.log('no Quote?');
                            this.standardQuote = true;
                             // do nothing
                     }
                     this.getProdSystemRecords(this.accountId); // Used to check if there are no Production System records associated to the account
                     this.getOrderLineData(this.orderId, this.accountId); // Get OrderLineItem records
                     this.isLoading = false;
                } else {
                    this.isLoading = false;
                    this.showCmp = false;
                    this.contractPresentValue = false;
                }
            })
            .catch(error => {
                this.isLoading = false;
                console.log('Error checking Contract: ', error);
            });

        }
    }

    // This will retrieve both System Tail and Production System records
    getProdSystemRecords(accountRecId){
        getProductionSystemRecords({
            accountId: accountRecId
        })
        .then(result => {
            // If there are multiple Product System records related to the Account
            if (result.length > 1){
                this.prodSystems = result;
                this.hasManyProdSystems = true;
                this.hasOneProdSystem = false;
                this.hasNoProdSystems = false;
            }
            // If there is only 1 Production System record related to the Account
            else if (result.length === 1){
                this.prodSystems = result;
                this.hasManyProdSystems = false;
                this.hasOneProdSystem = true;
                this.hasNoProdSystems = false;
            }
            // If there are NO Production System records related to the Account
            else {
                this.hasManyProdSystems = false;
                this.hasOneProdSystem = false;
                this.hasNoProdSystems = true;
                // Setting the following flags to have UI behave correctly
                this.showCmp = false;
                this.userChoice = true;
                this.standardQuote = true;
            }
        })
        .catch(error => {
           console.log('ERROR: ', error);
        });
    }



    closeQuickAction() {
         const closeQA = new CustomEvent('close');
         this.dispatchEvent(closeQA);
     }

     handleYes(){
            this.isLoading = true;
            this.standardQuote = true; // Need to set this for when we get to this screen from an Amendment Quote for it to work properly
            this.tailsDeregistered = false; // Need to set this for when we get to this screen from an Amendment Quote for it to work properly
            this.amendmentQuote = false;
            this.userSelectionMade = false;
            this.showCmp = false;
            this.userChoice = true;
         if (!this.hasNoProdSystems){
            // Need to build list of available Production System records (from OrderLineItems) to display as checkboxes
            // Gather all related Prod Systems for each OrderLine, will be used to build list of checkboxes for next user input screen
            let systemGroups = [];
            // The following code builds a list of checkbox items consisting of a value (Prod Sys recordId) and a lable (Prod System name)
            this.orderLineItems.forEach(item =>{
                if (systemGroups.indexOf(item.systemName) === -1) {
                    systemGroups.push(item.systemName);
                    let newCheckBox = new Object();
                    newCheckBox.label = item.systemName;
                    if (item.systemId.includes("MISSING ")) {
                        newCheckBox.value = "MISSING " + item.systemName;
                    } else {
                        newCheckBox.value = item.systemName;
                    };
                    this.checkboxArray.push(newCheckBox);
                }
            });
            this.isLoading = false;
         }
     }

     handleNo(){
         this.userChoice = false;
         this.showCmp = false;
     }

     handleBack(){
         this.checkboxArray = [];
         this.userSelectionMade = false;
         this.showCmp = true;
         this.userChoice = false;
     }

     getOrderLineData(inputOrderId, inputAccountId){
         getOrderLineData({
             orderId: inputOrderId,
             accountId: inputAccountId
         })
         .then(result => {
             console.log('result: ', result);
            this.orderLineItems = result;
         })
         .catch(error => {
             console.log('Get Order Items ERROR: ', error);
         })
     }

     handleMissingSystems(event){
         this.userSelectionMade = true;
         this.showMissingSelectionError = true;
         this.missingSelections = event.detail;
     }

     handleValidSelections(event){
         this.validSelections = event.detail;
         this.userSelectionMade = true;
         this.showMissingSelectionError = false;
     }

     handleUpdated(event){
         // Setting these 2 flags should render the activate order lwc
         this.showCmp = false;
         this.userChoice = false;
     }

     handleDeregistered(event){
         // Setting these to specific values, to have them go through the Standard Active Order path
         this.standardQuote = true;
         this.amendmentQuote = false;
         this.showCmp = true;
     }
}