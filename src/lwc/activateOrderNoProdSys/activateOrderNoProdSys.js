/**
 * Created by Matt on 3/20/2021.
 */

import { LightningElement } from 'lwc';
import NO_PROD_SYS_ERROR_MSG from '@salesforce/label/c.activateOrder_NoProdSysError';

export default class ActivateOrderNoProdSys extends LightningElement {
    noProdSysErrorMsg = NO_PROD_SYS_ERROR_MSG;

    closeQuickAction() {
             const closeQA = new CustomEvent('close');
             this.dispatchEvent(closeQA);
    }
}