/**
 * Created by Matt on 3/25/2021.
 */

import { LightningElement, api } from 'lwc';
import checkActivated from '@salesforce/apex/ActivateOrderLWC_Controller.checkActivated';
import getAllOrderItems from '@salesforce/apex/ActivateOrderLWC_Controller.getAllOrderItems';
import deactivateOldOrderItems from '@salesforce/apex/ActivateOrderLWC_Controller.deactivateOldOrderItems';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ActivateOrderAmendmentQuote extends LightningElement {
    @api orderid;
    oldOrderItems = [];
    newOrderItems = [];
    isLoading = true;
    isError = false;
    noRemovedOrderItems = false;

    connectedCallback(){
        // Check if order is activated already
        checkActivated({
            orderId : this.orderid
        })
        .then(resp => {
            this.isLoading = false;
            if (resp){
                this.isError = true;
                this.errorMessage = 'Order is already activated, no further action is required at this time.';
            } else {
               // Call Apex method to retrieve old and new order items
               getAllOrderItems({
                   orderId : this.orderid
               })
               .then(result => {
                   let oldTemp = [];
                   let newTemp = [];
                   // Loop through the result and separate the old and new order items into separate lists
                   result.forEach(item => {
                       console.log(item.isNew);
                       if (item.isNew){
                           newTemp.push(item);
                       } else {
                           oldTemp.push(item);
                       }
                   });
                   this.oldOrderItems = oldTemp;
                   this.newOrderItems = newTemp;
               })
               .catch(error => {
                   console.log('ERROR: ', error);
                   this.showToast('ERROR:', error.body.message, 'error');
               })

            }
        })
    }

    showToast(titleInput, msgInput, varInput){
        const event = new ShowToastEvent({
            title: titleInput,
            message: msgInput,
            variant: varInput
        });
        this.dispatchEvent(event);
     }

    closeQuickAction() {
             const closeQA = new CustomEvent('close');
             this.dispatchEvent(closeQA);
     }

     handleNext(){
         this.isLoading = true;
        // Build array of JSON objects to pass back to Apex to update the OrderItem records
        let recordsToDeactivate = [];
        this.oldOrderItems.forEach(item => {
            let jsonItem = new Object();
            jsonItem.recordId = item.id;
            recordsToDeactivate.push(jsonItem);
        });

        let oldOrderId;
        if (this.oldOrderItems){
            oldOrderId = this.oldOrderItems[0].orderId;
        }

        // Send list of JSON objects to Apex to update the OrderItems' Subscription records
        deactivateOldOrderItems({
            jsonInput : JSON.stringify(recordsToDeactivate),
            oldOrderId : oldOrderId
        })
        .then(result => {
            this.isLoading = false;
            if (result.isSuccess){
                this.showToast('','System Tails successfully de-registered.', 'success');
                this.dispatchEvent(new CustomEvent('deregistered'));
            } else {
                this.showToast('ERROR:',result.errorMsg,'error');
            }
        })
        .catch(error => {
            this.isLoading = false;
            console.log(error);
            this.showToast('ERROR:',error.body.message,'error');
        })

     }

}