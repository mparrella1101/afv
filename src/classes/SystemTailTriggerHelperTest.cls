/**
 * The test class for the 'SystemTailTriggerHelper' class and the 'SystemTailTrigger' Apex Trigger.
 *
 *
 * Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           May-14-2021     Creating initial version to cover 'SystemTailTriggerHelperTest' Apex Class and 'SystemTailTrigger' Apex Trigger
 */

@IsTest
public with sharing class SystemTailTriggerHelperTest {

    @TestSetup
    static void setupData(){
        // Create Test Account
        Account testAccount = new Account(
                Name = 'Apex Test Account',
                RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business_Account').getRecordTypeId()
        );

        insert testAccount;

        // Create Test Aircraft records
        List<ICAO_Model__c> aircraftModels = new List<ICAO_Model__c>();
        for (Integer i = 0; i < 3; i++){
            aircraftModels.add(new ICAO_Model__c(
                    Model__c = 'ApexAirliner ' + i,
                    Manufacturer__c = 'Salesforce ' + i,
                    TypeDesignator__c = 'TEST'
            ));
        }
        insert aircraftModels;

        // Create Tail__c records
        List<Tail__c> tails = new List<Tail__c>();
        for(Integer i = 0; i < 3; i++) {
            tails.add(new Tail__c(
                    Name = 'Apex Test Tail ' + i,
                    Aircraft__c = aircraftModels[i].Id,
                    Serial_Number__c = 'APEX_' + String.valueOf(Date.today())
            ));
        }
        insert tails;

        // Create Prod System records
        List<Production_System__c> prodsystems = new List<Production_System__c>();
        for(Integer i = 0; i < 3; i++) {
            if (i == 0){
                prodsystems.add(new Production_System__c(
                        Account__c = testAccount.Id,
                        System__c = 'APG'
                ));
            }
            else if (i == 1){
                prodsystems.add(new Production_System__c(
                        Account__c = testAccount.Id,
                        System__c = 'RocketRoute'
                ));
            }
            else if (i == 2) {
                prodsystems.add(new Production_System__c(
                        Account__c = testAccount.Id,
                        System__c = 'Seattle Avionics'
                ));
            }
        }
        insert prodsystems;

        // Create the Fleet Group records
        List<Fleet_Group__c> fleetGroups = new List<Fleet_Group__c>();
        for (Integer i = 0; i < 3; i++ ){
            fleetGroups.add(new Fleet_Group__c(
                    Customer__c = testAccount.Id,
                    Aircraft__c = aircraftModels[i].Id
            ));
        }
        insert fleetGroups;

        // Create System Tail Records
        List<System_Tail__c> systemTails = new List<System_Tail__c>();
        for (Integer i = 0; i < 3; i++) {
            systemTails.add(new System_Tail__c(
                    Name = 'Apex Placeholder',
                    Tail__c = tails[i].Id,
                    Production_System__c = prodsystems[i].Id,
                    Fleet_Group__c = fleetGroups[i].Id
            ));
        }
        insert systemTails;
    }

    @IsTest
    static void test_tailChangeCalc() {

        List<Tail__c> tails = [SELECT Id FROM Tail__c WHERE Name LIKE 'Apex Test Tail %'];
        System.assertEquals(3, tails.size());
        List<System_Tail__c> systemTails = [SELECT Id, Tail__c FROM System_Tail__c WHERE Tail__c IN :tails];
        System.assertEquals(3, systemTails.size());

        Test.startTest();
        System_Tail__c tempRecord = systemTails[1];
        Tail__c tempTail = tails[0];
        tempRecord.Tail__c = tempTail.Id;
        update tempRecord;
        Test.stopTest();
    }

    @IsTest
    static void test_sysTailDeletion() {
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account' LIMIT 1];
        List<Fleet_Group__c> fleetGroups = [SELECT Id FROM Fleet_Group__c WHERE Customer__c =: testAccount.Id];
        System.assertEquals(3, fleetGroups.size());
        List<System_Tail__c> sysTails = [SELECT Id, Fleet_Group__c FROM System_Tail__c WHERE Account_Name__c = 'Apex Test Account'];
        System.assertEquals(3, sysTails.size());

        // Assign FG to Sys Tails
        sysTails[0].Fleet_Group__c = fleetGroups[0].Id;
        sysTails[1].Fleet_Group__c = fleetGroups[1].Id;
        sysTails[2].Fleet_Group__c = fleetGroups[2].Id;

        update sysTails;

        Test.startTest();
        delete sysTails;
        Test.stopTest();
    }
}