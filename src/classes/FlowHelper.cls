/**
 *
 * Original - Coastal Cloud - Matt Parrella - 03-30-2021 - Creating intial version of class that is used in the 'Quote Acceptance Flow' to compare two currency values, looking for equality. For
 *                                                         whatever reason, this is the only way to get Salesforce to recognize that '$299.99' == '$299.99'. Very strange, but it works!
 *
 */

public with sharing class FlowHelper {
    public class ClassParams{
        @InvocableVariable(label='First value' Required=true)
        public Double firstVal;

        @InvocableVariable(label='Second value' Required = true)
        public Double secondVal;
    }

    @InvocableMethod(label='Used to compare 2 Double/Long values')
    public static List<String> isEqual(ClassParams[] classParameters){
        Decimal temp1 = Math.roundToLong(classParameters[0].firstVal);
        Decimal temp2 = Math.roundToLong(classParameters[0].secondVal);
        System.debug('***APEX RETURNING: ' + String.valueOf(temp1 == temp2));
        return new List<String>{ String.valueOf(temp1 == temp2) };
    }
}