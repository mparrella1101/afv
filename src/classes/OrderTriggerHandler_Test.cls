/**
 *  Test class for 'OrderTriggerHandler' class
 *
 *  02/16/2021 - Coastal Cloud - Matt Parrella - Creating initial version
 *  05/19/2021 - Coastal Cloud - Matt Parrella - Adding test coverage for new OrderItem trigger logic
 *
 */

@IsTest
public with sharing class OrderTriggerHandler_Test {
    // Get Legal Entity Account RTID
    public static Id LEGAL_ENTITY_RTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Legal_Entity').getRecordTypeId();

 
    @TestSetup
    static void setupData(){

        // Get Account 'Business Account' Record Type ID
        Id BUSINESS_ACCOUNT_RTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business_Account').getRecordTypeId();

        // Get Standard Pricebook Id
        Id PRICEBOOK_ID = Test.getStandardPricebookId();


        // Create Account
        Account testAccount = new Account(
                Name = 'Apex Test Account',
                RecordTypeId = BUSINESS_ACCOUNT_RTID,
                CurrencyIsoCode = 'USD'
        );

        insert testAccount;

        // Create Account Value by Proposition record
        Account_Value_Proposition__c testProposition = new Account_Value_Proposition__c(
           Account__c = testAccount.Id,
                Name = 'test prop'
        );

        insert testProposition;

        // Create some Product records
        List<Product2> productList = new List<Product2>();
        for (Integer i = 0; i < 3; i++) {
            Product2 prod = new Product2();
            prod.Name = 'Test Product ' + i;
            prod.IsActive = TRUE;
            productList.add(prod);
        }

        insert productList;

        // Create PricebookEntries
        List<PricebookEntry> PBEs = new List<PricebookEntry>();
        for (Integer i = 0; i < 3; i++){
            PBEs.add(new PricebookEntry(
               IsActive = TRUE,
                    CurrencyIsoCode = 'USD',
                    Product2Id = productList.get(i).Id,
                    Pricebook2Id = PRICEBOOK_ID,
                    UnitPrice = 100.00
            ));
        }

        insert PBEs;

        // Create some Aircraft Models
        List<ICAO_Model__c> aircraftModels = new List<ICAO_Model__c>();
        for (Integer i = 0; i < 3; i++){
            aircraftModels.add(new ICAO_Model__c(
               Model__c = 'ApexAirliner ' + i,
                    Manufacturer__c = 'Salesforce ' + i,
                    TypeDesignator__c = 'TEST'
            ));
        }
        try {
            insert aircraftModels;
        }
        Catch(DmlException e){System.debug('###: ' + e.getMessage());}
        // Create a few 'Value Proposition by Product' records
        List<Value_Proposition_by_Product__c> valuePropList = new List<Value_Proposition_by_Product__c>();
        for (Integer i = 0; i < 3; i++){
            valuePropList.add(new Value_Proposition_by_Product__c(
                    Product__c = productList.get(i).Id,
                    Aircraft__c = aircraftModels.get(i).Id,
                    Quantity__c = i + 1,
                    SubscriptionPerTailPerMonth__c = 100.00,
                    Account_Value_Proposition__c = testProposition.Id
            ));
        }
        insert valuePropList;

        // Create Contract
        Contract testContract = new Contract(
                AccountId = testAccount.Id,
                Status = 'Draft',
                StartDate = Date.today(),
                ContractTerm = 1
        );

        insert testContract;


        // Create Order record
        Order testOrder = new Order(
            AccountId = testAccount.Id,
                EffectiveDate = Date.today(),
                Contract = testContract,
                Status = 'Draft',
                CurrencyIsoCode = 'USD',
                Pricebook2Id = PRICEBOOK_ID
        );

        insert testOrder;


        // Create some OrderItem records
        List<OrderItem> orderItemList = new List<OrderItem>();
        aircraftModels = [SELECT Id, Manufacturer__c, Name FROM ICAO_Model__c WHERE Manufacturer__c LIKE 'Salesforce %'];
        productList = [SELECT Id FROM Product2 WHERE Name LIKE 'Test Product %'];
        for(Integer i = 0; i < 3; i++){
            orderItemList.add(new OrderItem(
               OrderId = testOrder.Id,
                    Aircraft__c =  aircraftModels[i].Name,
                    Product2Id = productList.get(i).Id,
                    ListPrice = 100.00,
                    UnitPrice = 100.00,
                    Quantity = 2,
                    PricebookEntryId = PBEs.get(i).Id
            ));
        }
        insert orderItemList;

    }
    /**
     * This method will test the 'OrderLineWrapper' wrapper class. Just ensuring all the values that are instantiated
     * in the constructor are working correctly.
     */

    @IsTest
    static void OrderLineWrapper_Class_Test(){
        Test.startTest();
        OrderTriggerHandler.OrderLineWrapper olw = new OrderTriggerHandler.OrderLineWrapper();
        Test.stopTest();
        System.assertEquals(0, olw.price, 'Price should be instantiated as 0, when creating a new wrapper object.');
        System.assertEquals(0, olw.quantity, 'Quantity should be instantiated as 0, when creating a new wrapper object.');
        System.assertEquals(false, olw.matchFound, 'This should be \'false\' by default, in order for logic to work properly.');
    }

    /**
     * This method will test the scenario where we have 3 OrderLineItems and 3 Value Proposition records that have the same
     * values for: Aircraft, Price, and Product. This scenario should NOT create any additional Value Proposition records, but
     * instead it should be updating the existing Value Proposition records' 'Quantity' field accordingly.
     *
     */

    @IsTest
    static void Matching_Records_Logic_Test(){
        Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Apex Test Account' LIMIT 1];

        // Update the order to 'Activated' to Trigger logic we're testing
        Order testOrder = [SELECT Id, Status FROM Order WHERE AccountId =: testAccount.Id];

        Test.startTest();
        testOrder.Status = 'Activated';
        update testOrder;
        Test.stopTest();

        // Assert that the quantity of each  Value by Proposition record increased by 1
        List<Value_Proposition_by_Product__c> valueProps = [SELECT Id, Quantity__c, Aircraft__r.Name FROM Value_Proposition_by_Product__c WHERE Account_Value_Proposition__r.Account__c =: testAccount.Id];
        System.assertEquals(3, valueProps.size(), 'Should only be 3 Value Proposition records, since they all should match the Order Items.');
        for (Value_Proposition_by_Product__c currProp : valueProps){
            if (currProp.Aircraft__r.Name == 'Test Aircraft 1'){
                System.assertEquals(3, currProp.Quantity__c, 'Quantity should\'ve increased by 2, since a match was found.');
            }
            if (currProp.Aircraft__r.Name == 'Test Aircraft 2'){
                System.assertEquals(4, currProp.Quantity__c, 'Quantity should\'ve increased by 2, since a match was found.');
            }
            if (currProp.Aircraft__r.Name == 'Test Aircraft 3'){
                System.assertEquals(5, currProp.Quantity__c, 'Quantity should\'ve increased by 2, since a match was found.');
            }
        }
    }

    /**
     *  This method will test the scenario where an OrderLineItem record is not matched to a Value Proposition record.
     *  In this scenario, it should be creating a new Value Proposition record for each OrderLineItem record that doesn't
     *  match. Since the test setup method will create 3 OrderLineItems and 3 Value Proposition records that match according
     *  to the parameters, we should be expecting just 1 new Value Proposition record to be created after this test executes.
     *
     */

    @IsTest
    static void No_Matching_Records_Logic_Test(){
        // Gather required record data to add a new Product/Order Item: START

        //Get Account
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account' LIMIT 1];

        // Get Related Order
        Order testOrder = [SELECT Id FROM Order WHERE AccountId =: testAccount.Id LIMIT 1];

        // Create Aircraft Model Record
        ICAO_Model__c newAircraft = new ICAO_Model__c(
                Name = 'Cessna 823',
                Manufacturer__c = 'Parrella Peoples'
        );

        insert newAircraft;

        // Create new Product2 record
        Product2 newProduct = new Product2(
           Name = 'New Aircraft',
                IsActive = TRUE
        );

        insert newProduct;

        // Create new PricebookEntry record
        PricebookEntry newPBE = new PricebookEntry(
                IsActive = TRUE,
                CurrencyIsoCode = 'USD',
                Product2Id = newProduct.Id,
                Pricebook2Id = Test.getStandardPricebookId(),
                UnitPrice = 1000.00
        );

        insert newPBE;

        // Gather required record data to add a new Product/Order Item: STOP


        // Create a new OrderItem record
        OrderItem newOI = new OrderItem(
                OrderId = testOrder.Id,
                Aircraft__c =  newAircraft.Id,
                Product2Id = newProduct.Id,
                ListPrice = 1000.00,
                UnitPrice = 1000.00,
                Quantity = 5,
                PricebookEntryId = newPBE.Id
        );

        insert newOI;

        // Activate the order to execute the trigger logic
        Test.startTest();
        testOrder.Status = 'Activated';
        update testOrder;
        Test.stopTest();

        // Assert results
        List<Value_Proposition_by_Product__c> valuePropResults = [SELECT Id, Aircraft__r.Name, Product__c, SubscriptionPerTailPerMonth__c, Quantity__c FROM Value_Proposition_by_Product__c WHERE Account_Value_Proposition__r.Account__c =: testAccount.Id];

        System.assertEquals(4, valuePropResults.size(), 'Should be 4 records, since 3 matched and we created a new one that did NOT match.');

        for (Value_Proposition_by_Product__c valueProp : valuePropResults){
            if (valueProp.Aircraft__r.Name == 'Cessna 823'){
                System.assertEquals(newProduct.Id, valueProp.Product__c, 'Products should match here.');
                System.assertEquals(newAircraft.Id, valueProp.Aircraft__c, 'Aircrafts should match here.');
                System.assertEquals(5, valueProp.Quantity__c, 'Quantity should match the OrderItem quantity.');
            }
        }
    }

    /**
     *  This test method will test the scenario where a Contract is Amended and Line Item quantities are changed. We should see
     *  the reflected Quantity value change on the Account's Value Prop record(s), if the trigger logic is performing correctly.
    */
    @IsTest
    static void amendment_Test1(){
        // Get 'Legal Entity' Account Record Type Id
        // Create Legal Entity Account
        Account legalEntityAccount = new Account(
                Name = 'RocketRoute',
                RecordTypeId = LEGAL_ENTITY_RTID,
                Website = 'https://www.rocketroute.com/terms',
                CurrencyIsoCode = 'USD'
        );

        insert legalEntityAccount;

        // Get Test Account record
        Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Apex Test Account' LIMIT 1];

        // Create Prod System record
        Production_System__c testProdSys = new Production_System__c(
                System__c = 'RocketRoute',
                CurrencyIsoCode = 'USD',
                Account__c = testAccount.Id
        );

        insert testProdSys;

        // Create Opportunity record
        Opportunity testOpp = new Opportunity(
                Name = 'Apex Test Opp',
                StageName = 'Working',
                CloseDate = Date.today().addDays(7)
        );

        insert testOpp;

        // Create Initial Quote
        SBQQ__Quote__c testQuote1 = new SBQQ__Quote__c(
                SBQQ__Type__c = 'Quote',
                SBQQ__Primary__c = true,
                SBQQ__SubscriptionTerm__c = 12.0,
                SBQQ__Status__c = 'Draft',
                SBQQ__Account__c = testAccount.Id,
                Legal_Entity__c = legalEntityAccount.Id,
                SBQQ__StartDate__c = Date.today(),
                SBQQ__EndDate__c = Date.today().addDays(365),
                SBQQ__Opportunity2__c = testOpp.Id,
                CurrencyIsoCode = 'USD'
        );

        insert testQuote1;

        // Add Line Item to Quote
        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Product 0' LIMIT 1];
        ICAO_Model__c testAircraft = [SELECT Id FROM ICAO_Model__c WHERE Manufacturer__c = 'Salesforce 0' LIMIT 1];

        SBQQ__QuoteLine__c newLine = new SBQQ__QuoteLine__c(
          SBQQ__Product__c = testProduct.Id,
                SBQQ__Quantity__c = 1.0,
                Aircraft__c = testAircraft.Id,
                SBQQ__ListPrice__c = 100.00,
                SBQQ__Quote__c = testQuote1.Id
        );

        insert newLine;


        // Can't call a screen-flow from a test class, so this point marks where the flow would start
        // 'Accept Quote & Register Lines V3' FLOW LOGIC: START

        // Set 'Contracted' checkbox on Opp to TRUE (needs to run as Admin because of 'NO_MASS_MAIL_PERMISSION' error)
        User adminUser = [SELECT Id FROM User WHERE IsActive = TRUE AND Profile.Name = 'System Administrator' LIMIT 1];
        System.runAs(adminUser){
            testOpp.SBQQ__Contracted__c = true;
            testOpp.SBQQ__PrimaryQuote__c = testQuote1.Id;
            update testOpp;
        }
        // Set Quote to 'Accepted'
        testQuote1.SBQQ__Status__c = 'Accepted';
        update testQuote1;



        // Create Order record
        Order testOrder = new Order(
                SBQQ__Quote__c = testQuote1.Id,
                Status = 'Draft',
                AccountId = testAccount.Id,
                EffectiveDate = testQuote1.SBQQ__StartDate__c,
                CurrencyIsoCode = testQuote1.CurrencyIsoCode,
                Opportunity = testOpp,
                Pricebook2Id = Test.getStandardPricebookId(),
                Type = 'New'
        );

        insert testOrder;

        // Create Order line
        OrderItem testOrderItem = new OrderItem(
                OrderId = testOrder.Id,
                Product2Id = newLine.SBQQ__Product__c,
                Quantity = newLine.SBQQ__Quantity__c,
                PricebookEntryId = [SELECT Id FROM PricebookEntry WHERE Product2Id =: testProduct.Id LIMIT 1].Id,
                UnitPrice = newLine.SBQQ__ListPrice__c,
                Aircraft__c = newLine.Aircraft__c,
                ServiceDate = newLine.SBQQ__StartDate__c,
                SBQQ__Activated__c = TRUE

        );

        insert testOrderItem;

        // Update Quote to include Order
        testQuote1.Order__c = testOrder.Id;
        update testQuote1;

        // Create contract
        Contract testContract = new Contract(
                Status = 'Draft',
                AccountId = testAccount.Id,
                SBQQ__Order__c = testOrder.Id,
                Pricebook2Id = Test.getStandardPricebookId(),
                SBQQ__Opportunity__c = testOpp.Id,
                SBQQ__Quote__c = testQuote1.Id,
                StartDate = Date.today(),
                ContractTerm = 12
        );

        insert testContract;

        // Activate order
        testContract.Status = 'Activated';
        testOrder.Status = 'Activated';
        update testContract;
        update testOrder;

        // Assert that a value Proposition record was created
        List<Value_Proposition_by_Product__c> resultVP = [SELECT Id, Quantity__c, Aircraft__c, SubscriptionPerTailPerMonth__c, Product__c FROM Value_Proposition_by_Product__c WHERE Account_Value_Proposition__c IN (SELECT Id FROM Account_Value_Proposition__c WHERE Account__c =: testAccount.Id)];
        System.assertEquals(4, resultVP.size(), 'Should be 4 records at this time. 3 are created in TestSetup, 1 is created in this test.');

        // 'Accept Quote & Register Lines V3' FLOW LOGIC: STOP

        // Create new Amendment Opp
        Opportunity amendOpp = new Opportunity(
            AccountId = testAccount.Id,
                Name = 'Amendment Test Opp',
                StageName = 'Working',
                CloseDate = Date.today()
        );

        insert amendOpp;

        // Creating new 'Amendment Quote'
        SBQQ__Quote__c amendQuote = new SBQQ__Quote__c(
                SBQQ__Type__c = 'Amendment',
                SBQQ__MasterContract__c = testContract.Id,
                SBQQ__Account__c = testAccount.Id,
                CurrencyIsoCode = 'USD'
        );

        insert amendQuote;

        // Edit Quote Lines
        SBQQ__QuoteLine__c amendLine = new SBQQ__QuoteLine__c(
                SBQQ__Product__c = testProduct.Id,
                SBQQ__Quantity__c = 3.0, // Increasing by 2
                Aircraft__c = testAircraft.Id,
                CurrencyIsoCode = 'USD',
                SBQQ__SubscriptionType__c = 'Renewable',
                SBQQ__StartDate__c = Date.today(),
                SBQQ__EndDate__c = Date.today().addDays(365),
                SBQQ__SubscriptionPricing__c = 'Fixed Price',
                SBQQ__ListPrice__c = 100.00,
                SBQQ__Quote__c = amendQuote.Id
        );

        try { insert amendLine; }
        Catch(DmlException e) { System.debug('****: ' + e.getMessage()); }

        // Create new Order for Amendment
        Order amendOrder = new Order(
                Status = 'Draft',
                AccountId = testAccount.Id,
                EffectiveDate = testQuote1.SBQQ__StartDate__c,
                CurrencyIsoCode = testQuote1.CurrencyIsoCode,
                Opportunity = testOpp,
                Pricebook2Id = Test.getStandardPricebookId(),
                Type = 'Amendment',
                ContractId = testContract.Id
        );
        insert amendOrder;

        // Create new Order line
        OrderItem amendOrderItem = new OrderItem(
                OrderId = amendOrder.Id,
                Product2Id = amendLine.SBQQ__Product__c,
                Quantity = amendLine.SBQQ__Quantity__c,
                PricebookEntryId = [SELECT Id FROM PricebookEntry WHERE Product2Id =: testProduct.Id LIMIT 1].Id,
                UnitPrice = amendLine.SBQQ__ListPrice__c,
                Aircraft__c = amendLine.Aircraft__c,
                ServiceDate = amendLine.SBQQ__StartDate__c,
                SBQQ__Activated__c = TRUE
        );

        insert amendOrderItem;

        amendQuote.Order__c = amendOrder.Id;
        amendQuote.SBQQ__Status__c = 'Accepted';
        update amendQuote;


        Test.startTest();
        amendOrder.Status = 'Activated';
        update amendOrder;
        Test.stopTest();

        List<Value_Proposition_by_Product__c> valueProps = [SELECT Id, Quantity__c, Aircraft__r.Name, Product__c, SubscriptionPerTailPerMonth__c FROM Value_Proposition_by_Product__c WHERE Account_Value_Proposition__c IN (Select Id FROM Account_Value_Proposition__c WHERE Account__c =: testAccount.Id)];
        for (Value_Proposition_by_Product__c vp : valueProps){
            System.debug('vp record: ' + vp);
            if (vp.Aircraft__r.Name == 'Test Aircraft 0'){
                System.assertEquals(3.0, vp.Quantity__c, 'Quantity of this record should\'ve increased by 2');
            }
        }
    }

    /**
     * Helper method
     */
    @IsTest
    static String getAccountId(){
        return [SELECT Id FROM Account WHERE Name = 'Apex Test Account' LIMIT 1].Id;
    }
    @IsTest
    static void coverThis(){
        OrderTriggerHandler.coverThis();
    }
}