/**
 * The Test class that covers both the 'FleetGroupTrigger' Trigger and 'FleetGroupTriggerHandler' class.
 *
 *
 * Version          Author                  Company                 Date                    Description
 --------------------------------------------------------------------------------------------------------------------------
    1.0             Matt Parrella           Coastal Cloud           May-11-2021             Creating initial version of class
 *
 */

@IsTest
public with sharing class FleetGroupTriggerHandlerTest {

    @TestSetup
    static void setupData(){
        // Create Test Account
        Account testAccount = new Account(
            Name = 'Apex Test Account',
                RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business_Account').getRecordTypeId()
        );

        insert testAccount;

        // Create Test Aircraft records
        List<ICAO_Model__c> aircraftModels = new List<ICAO_Model__c>();
        for (Integer i = 0; i < 3; i++){
            aircraftModels.add(new ICAO_Model__c(
                    Model__c = 'ApexAirliner ' + i,
                    Manufacturer__c = 'Salesforce ' + i,
                    TypeDesignator__c = 'TEST'
            ));
        }
        insert aircraftModels;

        // Create Tail__c records
        List<Tail__c> tails = new List<Tail__c>();
        for(Integer i = 0; i < 3; i++) {
            tails.add(new Tail__c(
               Name = 'Apex Test Tail ' + i,
                    Aircraft__c = aircraftModels[i].Id,
                    Serial_Number__c = 'APEX_' + String.valueOf(Date.today())
            ));
        }
        insert tails;

        // Create Prod System records
        List<Production_System__c> prodsystems = new List<Production_System__c>();
        for(Integer i = 0; i < 3; i++) {
            if (i == 0){
                prodsystems.add(new Production_System__c(
                    Account__c = testAccount.Id,
                        System__c = 'APG'
                ));
            }
            else if (i == 1){
                prodsystems.add(new Production_System__c(
                        Account__c = testAccount.Id,
                        System__c = 'RocketRoute'
                ));
            }
            else if (i == 2) {
                prodsystems.add(new Production_System__c(
                        Account__c = testAccount.Id,
                        System__c = 'Seattle Avionics'
                ));
            }
        }
        insert prodsystems;

        // Create the Fleet Group records
        List<Fleet_Group__c> fleetGroups = new List<Fleet_Group__c>();
        for (Integer i = 0; i < 3; i++ ){
            fleetGroups.add(new Fleet_Group__c(
                Customer__c = testAccount.Id,
                    Aircraft__c = aircraftModels[i].Id
            ));
        }
        insert fleetGroups;

        // Create System Tail Records
        List<System_Tail__c> systemTails = new List<System_Tail__c>();
        for (Integer i = 0; i < 3; i++) {
            systemTails.add(new System_Tail__c(
                    Name = 'Apex Placeholder',
                    Tail__c = tails[i].Id,
                    Production_System__c = prodsystems[i].Id,
                    Fleet_Group__c = fleetGroups[i].Id
            ));
        }
        insert systemTails;
    }

    @IsTest
    static void test_calculation(){
        List<Fleet_Group__c> fleetGroupsToUpdate = new List<Fleet_Group__c>();

        // First assert that the counts are 0
        for(Fleet_Group__c tempGroup : [SELECT Id, APG_Subscribed_Tails__c, RR_Subscribed_Tails__c, Seattle_Avionics_Subscribed_Tails__c, Count__c FROM Fleet_Group__c WHERE Customer__c IN (SELECT Id FROM Account WHERE Name = 'Apex Test Account')]){
            System.assertEquals(0, tempGroup.APG_Subscribed_Tails__c);
            System.assertEquals(0, tempGroup.RR_Subscribed_Tails__c);
            System.assertEquals(0, tempGroup.Seattle_Avionics_Subscribed_Tails__c);
            System.assertEquals(null, tempGroup.Count__c);
            tempGroup.Recalculate__c = true; // This will fire off the logic required to pass the asserts below
            fleetGroupsToUpdate.add(tempGroup);
        }

        // Start the test
        Test.startTest();
        update fleetGroupsToUpdate;
        Test.stopTest();

        // Assert counts have been updated accordingly
        for(Fleet_Group__c tempGroup : [SELECT Id, APG_Subscribed_Tails__c, Recalculate__c, RR_Subscribed_Tails__c, Seattle_Avionics_Subscribed_Tails__c, Count__c FROM Fleet_Group__c WHERE Customer__c IN (SELECT Id FROM Account WHERE Name = 'Apex Test Account')]){
            Boolean result = (tempGroup.APG_Subscribed_Tails__c != 0 || tempGroup.RR_Subscribed_Tails__c != 0 || tempGroup.Seattle_Avionics_Subscribed_Tails__c != 0) ? true : false;
            System.assertEquals(true, result);
            System.assertEquals(false, tempGroup.Recalculate__c);
        }
    }
}