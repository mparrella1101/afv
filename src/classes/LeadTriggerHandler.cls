/**
 *
 *  Original - Coastal Cloud - Matt Parrella - 03/18/2021 - Creating initial version of handler class for Lead Trigger
 *
 */

public with sharing class LeadTriggerHandler {
    /**
     * Class Vars
     */
    private static ID LEAD_PERSON_ACCOUNT_RTID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Person_Account_Lead').getRecordTypeId();


    /**
     * This method contains all methods that should be executed in an 'After Insert' context
     *
     * @param newRecords - List of recently created Lead record(s)
     * @return void
     */
    public static void handleAfterInsert(List<Lead> newRecords){

    }

    /**
     * This method contains all methods that should be executed in an 'After Update' context
     * @param newRecords - List of modified Lead record(s)
     * @param oldRecordMap - Map containing old version(s) of Lead record(s) (before modification)
     */
    public static void handleAfterUpdate(List<Lead> newRecords, Map<Id,Lead> oldRecordMap){
        /* Loop through new Lead records and check the following conditions for 'autoConvertLead' method:
                - RecordType = 'Person Account Lead'
                - Auto_Converted__c field is changed to TRUE
        */
        List<Lead> leadsToProcess = new List<Lead>();
        for (Lead currLead : newRecords){
            if (currLead.RecordTypeId == LEAD_PERSON_ACCOUNT_RTID && (currLead.Auto_Converted__c == TRUE && currLead.Auto_Converted__c != oldRecordMap.get(currLead.Id).Auto_Converted__c)){
                leadsToProcess.add(currLead);
            }
        }
        if (!leadsToProcess.isEmpty()){
            autoConvertLead(leadsToProcess);
        }
    }


    /**
     * This method will convert the passed-in list of Lead records into Person Accounts and associate any subscription records on the Lead to the new Person Account
     *
     * @param newLeads - List of Lead records that meet the criteria of RecordType = 'Person Account Lead' and 'Auto_Converted__c' is changed to TRUE
     * @return void
     */
    private static void autoConvertLead(List<Lead> newLeads){
        List<Database.LeadConvert> leadConvertList = new List<Database.LeadConvert>(); // List to hold LeadConvert records (Leads that will be converted)
        Map<Id,List<SBQQ__Subscription__c>> leadSubscriptionMap = new Map<Id,List<SBQQ__Subscription__c>>(); // Map to hold Lead Record Id -> Related Subscription records (used for relating these Subscription records to the new account created)

        // Build Lead Id -> List of Subscription records map
        for(SBQQ__Subscription__c subscription : [SELECT Id, Lead__c FROM SBQQ__Subscription__c WHERE Lead__c IN (SELECT Id FROM Lead WHERE Id IN :newLeads)]){
            if (!leadSubscriptionMap.containsKey(subscription.Lead__c)){
                leadSubscriptionMap.put(subscription.Lead__c, new List<SBQQ__Subscription__c>{ subscription });
            } else {
                leadSubscriptionMap.get(subscription.Lead__c).add(subscription);
            }
        }

        // Get the conversion status
        String convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1].MasterLabel;

        // Loop through new Lead records and create LeadConvert records
        for (Lead currLead : newLeads){
            Database.LeadConvert tempConvert = new Database.LeadConvert();
            tempConvert.setLeadId(currLead.Id);
            tempConvert.setConvertedStatus(convertStatus);
            tempConvert.doNotCreateOpportunity = true;

            leadConvertList.add(tempConvert);
        }

        if (!leadConvertList.isEmpty()){
            List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>(); // Holds all CPQ subscription records that need to be re-parented to the new account
            List<Database.LeadConvertResult> results = Database.convertLead(leadConvertList); // Convert the Leads and store the results

            // Loop through results, get each AccountId and update subscription records
            for (Database.LeadConvertResult res : results){
                if (res.isSuccess() && leadSubscriptionMap.containsKey(res.getLeadId())){
                    String newAccountId = res.getAccountId();
                    for (SBQQ__Subscription__c sub : leadSubscriptionMap.get(res.getLeadId())){
                        sub.SBQQ__Account__c = newAccountId;
                        subscriptionsToUpdate.add(sub);
                    }
                }
            }
            if (!subscriptionsToUpdate.isEmpty()){
                try {
                    update subscriptionsToUpdate;
                }
                Catch(DmlException e){
                    System.debug('***Error occurred within LeadTriggerHandler.autoConvertLead: ' + e.getMessage());
                }
            }
        }
    }
}