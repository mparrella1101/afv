/**
 * The Apex Trigger Helper class for the System_Tail__c Trigger. All trigger logic will be contained within this class.
 *
 *
 * Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           May-14-2021     Creating initial version in order to keep Fleet_Group__c count fields up-to-date in real-time
    2.0         Matt Parrella           Coastal CLoud           June-04-2021    Adding method to handle Fleet Group counts when Sys Tail record is deleted
 */

public with sharing class SystemTailTriggerHelper {

    /**
     *  @description This method will handle all 'after update' Trigger execution context logic. From here, custom methods will be
     *               called and executed.
     *
     *  @param newRecords - List<System_Tail__c> - A List of recently modified System_Tail__c records, containing the most recently input values (equivalent to Trigger.new)
     *  @param oldMap - Map<Id,System_Tail__c> - A Map containing all the recently modified System_Tail__c records, but with the PREVIOUS field values present (equivalent to Trigger.oldMap)
     *
     *  @return void
    */
    public static void handleAfterUpdate(List<System_Tail__c> newRecords, Map<Id,System_Tail__c> oldMap){
        Set<Id> tailIds = new Set<Id>();
        // Gather the Tail__c Record IDs that were either added or removed from the System_Tail__c record
        for (System_Tail__c tempTail : newRecords){
            if (tempTail.Tail__c != oldMap.get(tempTail.Id).Tail__c){
                if (tempTail.Tail__c != null){
                    tailIds.add(tempTail.Tail__c);
                }
                if (oldMap.get(tempTail.Id).Tail__c != null){
                    tailIds.add(oldMap.get(tempTail.Id).Tail__c);
                }
            }
        }
        recalculateFleetGroups(tailIds, null); // Call to set 'Recalculate__c' flag on related Fleet_Group__c records
    }

    /**
     * @description  This method will handle all 'after delete' Trigger execution context logic. From here, custom methods will be
     *               called and executed.
     *
     * @param oldRecords - List<System_Tail__c> - A list of deleted System_Tail__c records
     *
     * @return void
     *
     */
    public static void handleAfterDelete(List<System_Tail__c> oldRecords){
        Set<Id> fleetGroupIds = new Set<Id>();
        for(System_Tail__c tempTail :oldRecords){
            if (tempTail.Fleet_Group__c != null){
                fleetGroupIds.add(tempTail.Fleet_Group__c);
            }
        }
        recalculateFleetGroups(null, fleetGroupIds);
    }



    /**
     *  @description This method will take in a list of System_Tail__c records that have just had the 'Tail__c' field value changed. It will
     *               query for all related Fleet_Group__c records, using both the previous and new Tail__c field value(s) (if they're not null).
     *               With the Fleet_Group__c records, it will iterate through each of them and check the 'Recalculate__c' checkbox to TRUE, where
     *               the FleetGroupTrigger logic will take over. Either 'tailIds' will be passed-in or 'sysTailIds' BUT NOT BOTH
     *
     *  @param tailIds - Set<Id> - A set of all Tail__c record Ids that have been either added or removed from a System_Tail__c record (these will be used
     *                                   to query for All related System_Tail__c records)
     *  @param fleetGroupIds - Set<Id> - A set of Fleet Group Record Ids that require recalculation
     *
     *  @return void
     */
    private static void recalculateFleetGroups(Set<Id> tailIds, Set<Id> fleetGroupIds){
        if (tailIds == null && fleetGroupIds == null){
            return;
        }

        List<Fleet_Group__c> recordsToUpdate = new List<Fleet_Group__c>();
        
        // Iterate over related Fleet_Group__c records and check the 'Recalculate__c' checkbox (if tailIds were passed-in)
        if (tailIds != null && tailIds?.isEmpty()){
            for (Fleet_Group__c tempFG : [SELECT Id, Recalculate__c FROM Fleet_Group__c WHERE Id IN (SELECT Fleet_Group__c FROM System_Tail__c WHERE Tail__c IN :tailIds)]){
                tempFG.Recalculate__c = true;
                recordsToUpdate.add(tempFG);
            }
        // If fleetGroupIds were provided
        } else if (fleetGroupIds != null && !fleetGroupIds?.isEmpty()){
            for (Fleet_Group__c tempFG : [SELECT Id, Recalculate__c FROM Fleet_Group__c WHERE Id IN :fleetGroupIds]){
                tempFG.Recalculate__c = true;
                recordsToUpdate.add(tempFG);
            }
        }

        if (!recordsToUpdate.isEmpty()){
            try {
                update recordsToUpdate;
            }
            Catch(DMLException e) {
                System.debug('*** Error updating Fleet_Group__c records from the \'SystemTailTriggerHelper\' class: ' + e.getMessage());
            }
        }
    }
}