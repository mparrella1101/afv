/**
 *
 * Original - Coastal Cloud - Matt Parrella - 03/28/2021 - Creating initial version of test class to cover code within 'ActivateOrderLWC_Controller' class
 *
 */

@IsTest
public with sharing class ActivateOrderLWC_Controller_Test {

    // Setup Test Data
    @TestSetup
    static void setup_data(){

        // Get Account 'Business Account' Record Type ID
        Id BUSINESS_ACCOUNT_RTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business_Account').getRecordTypeId();

        // Get Account 'Legal Entity' REcord Type ID
        Id LEGAL_ENTITY_RTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Legal_Entity').getRecordTypeId();

        // Get Standard Pricebook Id
        Id PRICEBOOK_ID = Test.getStandardPricebookId();


        // Create Account
        Account testAccount = new Account(
                Name = 'Apex Test Account',
                RecordTypeId = BUSINESS_ACCOUNT_RTID,
                CurrencyIsoCode = 'USD'
        );

        insert testAccount;

        Account legalAccount = new Account(
                Name = 'Apex Legal Account Test',
                RecordTypeId = LEGAL_ENTITY_RTID,
                CurrencyIsoCode = 'USD'
        );
        insert legalAccount;


        // Create Production System record
        Production_System__c testSystem = new Production_System__c(
                System__c = 'RocketRoute',
                Account__c = testAccount.Id
        );
        insert testSystem;

        // Create some Product records
        List<Product2> productList = new List<Product2>();
        for (Integer i = 0; i < 3; i++) {
            Product2 prod = new Product2();
            prod.Name = 'Test Product ' + i;
            prod.Family = 'RocketRoute';
            prod.IsActive = TRUE;
            productList.add(prod);
        }

        insert productList;

        // Create PricebookEntries
        List<PricebookEntry> PBEs = new List<PricebookEntry>();
        for (Integer i = 0; i < 3; i++){
            PBEs.add(new PricebookEntry(
                    IsActive = TRUE,
                    CurrencyIsoCode = 'USD',
                    Product2Id = productList.get(i).Id,
                    Pricebook2Id = PRICEBOOK_ID,
                    UnitPrice = 100.00
            ));
        }

        insert PBEs;

        // Create some Aircraft Models
        List<ICAO_Model__c> aircraftModels = new List<ICAO_Model__c>();
        for (Integer i = 0; i < 3; i++){
            aircraftModels.add(new ICAO_Model__c(
                    Name = 'Test Aircraft ' + i,
                    Manufacturer__c = 'Manufacturer ' + i
            ));
        }

        insert aircraftModels;

        // Create Tail records
        List<Tail__c> tails = new List<Tail__c>();
        for (Integer i = 0; i < 3; i++){
            tails.add(new Tail__c(
               Name = 'Tail ' + i,
                    Aircraft__c = aircraftModels[i].id
            ));
        }
        insert tails;


        // Create System Tail records
        List<System_Tail__c> systemTails = new List<System_Tail__c>();
        for (Integer i = 0; i < 3; i++){
            systemTails.add(new System_Tail__c(
                    Name = 'System Tail ' + i,
                    Tail__c = tails[i].Id,
                    Production_System__c = testSystem.Id,
                    Tail_Aircraft_Name__c = 'Test ' + i,
                    Active__c = TRUE
                    ));
        }
        try {
            insert systemTails;

        } catch(DmlException e) { System.debug(e.getMessage());}


        // Create Contract
        Contract testContract = new Contract(
                AccountId = testAccount.Id,
                Status = 'Draft',
                StartDate = Date.today(),
                ContractTerm = 1
        );

        insert testContract;


        // Create Order record
        Order testOrder = new Order(
                AccountId = testAccount.Id,
                EffectiveDate = Date.today(),
                ContractId = testContract.Id,
                Status = 'Draft',
                CurrencyIsoCode = 'USD',
                Pricebook2Id = PRICEBOOK_ID
        );

        insert testOrder;

        Order oldOrder = new Order(
                AccountId = testAccount.Id,
                EffectiveDate = Date.today().addDays(10),
                ContractId = testContract.Id,
                Status = 'Draft',
                CurrencyIsoCode = 'USD',
                Pricebook2Id = PRICEBOOK_ID
        );
        insert oldOrder;


        // Create some OrderItem records for new order
        List<OrderItem> orderItemList = new List<OrderItem>();
        aircraftModels = [SELECT Id, Manufacturer__c FROM ICAO_Model__c WHERE Manufacturer__c LIKE 'Manufacturer %'];
        productList = [SELECT Id FROM Product2 WHERE Name LIKE 'Test Product %'];
        for(Integer i = 0; i < 3; i++){
            orderItemList.add(new OrderItem(
                    OrderId = testOrder.Id,
                    Aircraft__c =  aircraftModels.get(i).Manufacturer__c + ' -  -',
                    Product2Id = productList.get(i).Id,
                    ListPrice = 100.00,
                    UnitPrice = 100.00,
                    Quantity = 2,
                    PricebookEntryId = PBEs.get(i).Id
            ));
        }
        insert orderItemList;

        // Create some OrderItem records for old order
        List<OrderItem> orderItemList2 = new List<OrderItem>();
        aircraftModels = [SELECT Id, Manufacturer__c FROM ICAO_Model__c WHERE Manufacturer__c LIKE 'Manufacturer %'];
        productList = [SELECT Id FROM Product2 WHERE Name LIKE 'Test Product %'];
        for(Integer i = 0; i < 3; i++){
            orderItemList2.add(new OrderItem(
                    OrderId = oldOrder.Id,
                    Aircraft__c =  aircraftModels.get(i).Manufacturer__c + ' -  -',
                    Product2Id = productList.get(i).Id,
                    ListPrice = 100.00,
                    UnitPrice = 100.00,
                    Quantity = 2,
                    PricebookEntryId = PBEs.get(i).Id
            ));
        }
        insert orderItemList2;

    }

    @IsTest
    static void getProductionSystems_test(){
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];
        List<Production_System__c> prodSystems = ActivateOrderLWC_Controller.getProductionSystems(testAccount.Id);

        // Assert results
        System.assertEquals('RocketRoute', prodSystems[0].System__c);
        System.assertEquals(testAccount.Id, prodSystems[0].Account__c);
    }

    @IsTest
    static void getProductSystemRecords_test(){
        Account testAccount = [SELECT Id FROM Account WHERE NAme = 'Apex Test Account'];
        List<ActivateOrderLWC_Controller.ProdSystemWrapper> prodSystemWrappers = ActivateOrderLWC_Controller.getProductionSystemRecords(testAccount.Id);

        // Assert results
        System.assertEquals('RocketRoute', prodSystemWrappers[0].systemName);
        System.assertEquals(testAccount.Id, prodSystemWrappers[0].accountId);
    }

    @IsTest
    static void getOrderLineData_test(){
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];
        Order testOrder = [SELECT Id FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today()];
        List<ActivateOrderLWC_Controller.OrderLineItemWrapper> orderLineWrappers = ActivateOrderLWC_Controller.getOrderLineData(testOrder.Id, testAccount.Id);

        // Assert results
        for (ActivateOrderLWC_Controller.OrderLineItemWrapper wrap : orderLineWrappers){
            System.assertNotEquals(null, wrap.productId);
            System.assertNotEquals(null, wrap.productName);
            System.assertNotEquals(null, wrap.systemName);
            System.assertNotEquals(null, wrap.aircraft);
        }
    }

    @IsTest
    static void getSpecificOrderLineData_test(){
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];
        Order testOrder = [SELECT Id FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today()];
        List<String> prodSystems = new List<String>{'RocketRoute'};

        List<ActivateOrderLWC_Controller.OrderLineItemWrapper> orderLineWraps = ActivateOrderLWC_Controller.getSpecificOrderLineData(testOrder.Id, testAccount.Id, prodSystems);

        // Assert results
        for(ActivateOrderLWC_Controller.OrderLineItemWrapper wrap : orderLineWraps){
            System.assertNotEquals(null, wrap.productId);
            System.assertNotEquals(null, wrap.orderId);
            System.assertNotEquals(null, wrap.aircraft);
            System.assertNotEquals(null, wrap.availableTails);
        }
    }

    @IsTest
    static void updateOrderLineItems_test(){
        // Get OrderLine record Id
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];
        Order testOrder = [SELECT Id, (SELECT Id FROM OrderItems) FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today()];
        String orderItemId = testOrder.OrderItems[0]?.Id;

        String tailId = [SELECT Id FROM System_Tail__c WHERE Tail_Aircraft_Name__c LIKE 'Test %' LIMIT 1].Id;

        // Build JSON string
        String jsonInput = '[{"recordId":"' + orderItemId + '","tailId":"' + tailId + '"}]';

        // Call the method
        ActivateOrderLWC_Controller.ResponseWrapper resp = ActivateOrderLWC_Controller.updateOrderLineItems(jsonInput);

        // Assert was successful
        System.assertEquals(true, resp.isSuccess);
        System.assertEquals('', resp.errorMsg);
    }

    @IsTest
    static void activateRecords_test(){
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];
        Order testOrder = [SELECT Id, (SELECT Id FROM OrderItems) FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today()];

        ActivateOrderLWC_Controller.ResponseWrapper resp = ActivateOrderLWC_Controller.activateRecords(testOrder.Id);

        // Assert was successful
        testOrder = [SELECT Id, Status, ContractId FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today()];
        Contract testContract = [SELECT Id, Status FROM Contract WHERE Id =: testOrder.ContractId];
        System.assertEquals('Activated', testContract.Status);
        System.assertEquals('Activated', testOrder.Status);
        System.assertEquals(true, resp.isSuccess);
        System.assertEquals('', resp.errorMsg);
    }

    @IsTest
    static void checkActivated_test(){
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];
        String orderId = [SELECT Id FROM Order WHERE AccountId = :testAccount.Id AND EffectiveDate =: Date.today()].Id;

        Boolean result = ActivateOrderLWC_Controller.checkActivated(orderId);
        System.assertEquals(false, result);
    }

    @IsTest
    static void getNewOrderItems_test(){
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];
        Order testOrder = [SELECT Id FROM Order WHERE AccountId = :testAccount.Id AND EffectiveDate =: Date.today()];

        List<ActivateOrderLWC_Controller.OrderLineItemWrapper> orderItemWrappers = ActivateOrderLWC_Controller.getNewOrderItems(testOrder.id);
        System.assertEquals(3, orderItemWrappers.size());
    }

    @IsTest
    static void deactivateOldOrderItems_test(){
        // Get OrderLine record Id
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];
        Order testOrder = [SELECT Id, ContractId, (SELECT Id FROM OrderItems) FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today()];
        Contract testContract = [SELECT Id, Status FROM Contract WHERE Id =: testOrder.ContractId LIMIT 1];
        testContract.Status = 'Activated';
        update testContract;
        String orderItemId = testOrder.OrderItems[0]?.Id;

        String tailId = [SELECT Id FROM System_Tail__c WHERE Tail_Aircraft_Name__c LIKE 'Test %' LIMIT 1].Id;

        // Build JSON string
        String jsonInput = '[{"recordId":"' + orderItemId + '","tailId":"' + tailId + '"}]';

        ActivateOrderLWC_Controller.ResponseWrapper resp = ActivateOrderLWC_Controller.deactivateOldOrderItems(jsonInput, testOrder.Id);

        // Assert results
        OrderItem testOrderItem = [SELECT Id, System_Tail__c FROM OrderItem WHERE Id =: orderItemId];
        System.assertEquals(null, testOrderItem.System_Tail__c);
        System.assertEquals(true, resp.isSuccess);
        System.assertEquals('', resp.errorMsg);
    }

    @IsTest
    static void getOldOrderItems_test(){
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];

        // Get the old order
        Order oldOrder = [SELECT Id FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today().addDays(10)];

        // Get the new order
        Order testOrder = [SELECT Id, ContractId FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today()];

        // Get PRod System
        Production_System__c prodSys = [SELECT Id FROM Production_System__c WHERE System__c = 'RocketRoute'];
        // Create opp
        Opportunity testOpp = new Opportunity(
                Name = 'Test Opp',
                StageName = 'Working',
                AccountId = testAccount.Id,
                CurrencyIsoCode = 'USD',
                CloseDate = Date.today().addDays(30)
        );
        insert testOpp;



        // Create Legal Entity
        Account legalAccount = [SELECT Id FROM Account WHERE Name = 'Apex Legal Account Test'];

        // Create a Quote
        SBQQ__Quote__c newQuote = new SBQQ__Quote__c();
        newQuote.SBQQ__Opportunity2__c = testOpp.Id;
        newQuote.SBQQ__Account__c = testAccount.Id;
        newQuote.SBQQ__Type__c = 'Quote';
        newQuote.SBQQ__Status__c = 'Draft';
        newQuote.Legal_Entity__c = legalAccount.Id;
        newQuote.SBQQ__MasterContract__c = [SELECT Id FROM Contract WHERE Id =: testOrder.ContractId].Id;
        newQuote.Order__c = testOrder.Id;
        insert newQuote;

        // Update the contract
        Contract testContract = [SELECT Id, SBQQ__Quote__c FROM Contract WHERE Id =: testOrder.ContractId];
        testContract.SBQQ__Quote__c = newQuote.Id;

        update testContract;

        testOrder.SBQQ__Quote__c = newQuote.Id;
        update testOrder;

        List<ActivateOrderLWC_Controller.OrderLineItemWrapper> wraps = ActivateOrderLWC_Controller.getOldOrderItems(testOrder.Id);
        System.assertEquals(3, wraps.size());
        for (ActivateOrderLWC_Controller.OrderLineItemWrapper wrap : wraps){
            System.assertNotEquals(null, wrap.productId);
        }
    }

    @IsTest
    static void getOldOrderItems_NoOrigQuote(){
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Apex Test Account'];

        // Get the new order
        Order testOrder = [SELECT Id, (Select Id, Aircraft__c, Product2Id FROM OrderItems), ContractId FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today()];


        Contract testContract = [SELECT Id FROM Contract WHERE Id =: testOrder.ContractId];

        // Get the old order
        Order oldOrder = [SELECT Id FROM Order WHERE AccountId =: testAccount.Id AND EffectiveDate =: Date.today().addDays(10)];
        oldOrder.ContractId = testContract.Id;

        update oldOrder;

        // Get PRod System
        Production_System__c prodSys = [SELECT Id FROM Production_System__c WHERE System__c = 'RocketRoute'];
        // Create opp
        Opportunity testOpp = new Opportunity(
                Name = 'Test Opp',
                StageName = 'Working',
                AccountId = testAccount.Id,
                CurrencyIsoCode = 'USD',
                CloseDate = Date.today().addDays(30)
        );
        insert testOpp;

        String aircraftName = [SELECT Id FROM ICAO_Model__c WHERE Name =: testOrder.OrderItems[0].Aircraft__c LIMIT 1].Id;

        // Create Subscription record for old order item
        SBQQ__Subscription__c newSub = new SBQQ__Subscription__c(
                SBQQ__Account__c = testAccount.Id,
                SBQQ__Product__c = testOrder.OrderItems[0].Product2Id,
                SBQQ__Contract__c = testContract.Id,
                Aircraft__c = aircraftName,
                SBQQ__Quantity__c = 1.0
        );
        insert newSub;

        testOrder.OrderItems[0].SBQQ__Subscription__c = newSub.Id;
        update testOrder.OrderItems[0];

        List<ActivateOrderLWC_Controller.OrderLineItemWrapper> wraps = ActivateOrderLWC_Controller.getOldOrderItems(testOrder.Id);
        System.assertEquals(3, wraps.size());
        for (ActivateOrderLWC_Controller.OrderLineItemWrapper wrap : wraps){
            System.assertNotEquals(null, wrap.productId);
        }
    }
}