/**
 *  Trigger Handler class for the Order Trigger
 *
 *
 *  02/11/2021 - Coastal Cloud - Matt Parrella - Creating initial version
 *  05/19/2021 - Coastal Cloud - Matt Parrella - Adding logic to populate OrderItem's Subscription fields on Activating Amendment Order record
 */

public with sharing class OrderTriggerHandler {

    // Wrapper class used for creating/updating Value Prop records
    public class OrderLineWrapper {
        public String productId; // stores id of product
        public String aircraftId; // stores id of aircraft
        public double price; // unit price
        public double quantity; // used for counting how many matches between OrderItem and Account_Value_Propositionc__c records were found
        public String accountPropId; // Id of Account Value Prop record
        public String valuePropId; // Used in a scenario where there is a match and we need to update the quantity field of a Value Proposition by Product record
        public String currencyCode; // Used to hold the CurrencyISOCode for each account
        public Boolean matchFound; // Used to indicate when an OrderItem has been matched with a Value Prop record, based on criteria
        public OrderLineWrapper(){
            this.productId = '';
            this.aircraftId = '';
            this.price = 0.00;
            this.quantity = 0.00;
            this.accountPropId = '';
            this.valuePropId = '';
            this.currencyCode = '';
            this.matchFound = false;
        }
    }

    /**
     * @description This method will handle all After Update trigger context logic
     *
     * @param newRecords - List of modified Order records
     * @param oldMap  - Map consisting of Id -> Order record, representing all previous versions of modified Order records
     *
     * @return void
     */
    public static void handleAfterUpdate(List<Order> newRecords, Map<Id,Order> oldMap){
        List<Order> ordersToProcess = new List<Order>(); // List of Order records that have been activated
        List<Order> amendmentOrders = new List<Order>(); // List of Orders that are of type 'Amendment'
        for(Order o : newRecords){
            // Check if Status was changed to 'Activated'
            if (o.Status == 'Activated' && (o.Status != oldMap.get(o.Id).Status)){
                if (o.Type != 'Amendment') {
                    ordersToProcess.add(o);
                } else {
                    amendmentOrders.add(o);
                }
            }
        }
        if (!ordersToProcess.isEmpty()){
            checkValuePropByProductRecords(ordersToProcess);
        }
        if (!amendmentOrders.isEmpty()){
            amendment_checkValueByPropRecords(amendmentOrders);
            populateSubscriptions(amendmentOrders);
        }
    }


    /**
     * @description This method will take in a list of Order records who's status has been changed to 'Activated'. It will then build a
     *              list of related Order Line records (grouped by Product, Aircraft, & Price). This records in this newly created list
     *              will then be compared against the related Account's "Value Prop by Product" records. If there is a match on Product,
     *              Aircraft, and Price, we will update the 'Quantity' field of the "Value Prop by Product" record. If no matches are found,
     *              we will be creating a new "Value Prop by Product" record.
     *
     * @param newRecords - List of Order records that have just had their Status changed to 'Activated'
     *
     * @return void
     */
    private static void checkValuePropByProductRecords(List<Order> newRecords){
        if (newRecords.isEmpty()){
            return;
        }
        Set<Id> accountIds = new Set<Id>(); // Set to hold all AccountId values from Order records
        List<OrderLineWrapper> orderWrapperList = new List<OrderLineWrapper>(); // List to hold all OrderLineWrapper records for processing
        Map<String,String> aircraftMap = getAircraftMap();

        Map<String, String> acctPropMap = new Map<String,String>(); // This map will hold Account Id -> related Account Value Prop Id (Used for associating a new Value Prop record to the correct account)
        List<Order> ordersList = [SELECT Id, AccountId, Type, Opportunity.Type FROM Order WHERE Id IN :newRecords];
        // Get all AccountIds
        for (Order o : ordersList){
            if (o.AccountId != NULL && o.Opportunity.Type != 'Renewal'){
                accountIds.add(o.AccountId);
            }
        }
        // Build map of AccountId -> Account Value Prop Record Id (used when creating a new Value Prop record)
        for (Account_Value_Proposition__c acctProp : [SELECT Id, Account__c FROM Account_Value_Proposition__c WHERE Account__c IN :accountIds]){
            acctPropMap.put(acctProp.Account__c, acctProp.Id);
        }

        // Get all Value Prop by Product records
        List<Value_Proposition_by_Product__c> vpList = [SELECT Id, Aircraft__r.Name, Aircraft__r.Id, Account_Value_Proposition__c, Account_Value_Proposition__r.Account__r.CurrencyIsoCode, Product__c, SubscriptionPerTailPerMonth__c, Quantity__c FROM Value_Proposition_by_Product__c WHERE Account_Value_Proposition__r.Account__c IN :accountIds];

        // Build Map Id -> Value Prop record (for updating existing records)
        Map<Id,Value_Proposition_by_Product__c> vpMap = new Map<Id,Value_Proposition_by_Product__c>();
        for (Value_Proposition_by_Product__c vp : vpList){
            vpMap.put(vp.Id, vp);
        }

        // Loop through Order Items and Value Prop list to look for matches on AircraftId, Product2Id and UnitPrice
        // O(n^2) :(
        for(OrderItem oi : [SELECT Id, Aircraft__c, Quantity, Product2Id, CurrencyIsoCode, UnitPrice, Order.AccountId FROM OrderItem WHERE OrderId IN :newRecords]){
            OrderLineWrapper olw = new OrderLineWrapper();
            olw.accountPropId = acctPropMap.get(oi.Order.AccountId);
            olw.currencyCode = oi.CurrencyIsoCode;
            for (Value_Proposition_by_Product__c vp : vpList){
                // If we have all 3 matching update the OrderLineWrapper fields accordingly (we won't be creating a new Value Prop record in this case,
                // but instead, we'll be updating existing Value Prop record's 'Quantity' field value
                if (oi.Aircraft__c == vp.Aircraft__r.Name && oi.Product2Id == vp.Product__c && oi.UnitPrice == vp.SubscriptionPerTailPerMonth__c){
                    olw.aircraftId = vp.Aircraft__r.Id;
                    olw.productId = oi.Product2Id;
                    olw.price = oi.UnitPrice;
                    olw.valuePropId = vp.Id; // Need to store this so we know which record to update the quantity on
                    olw.quantity += oi.Quantity;
                    olw.matchFound = true; // Used later to indicate we're updating an existing record, instead of creating a new record
                }
            } // End inner loop
            // If olw.quantity is 0, that means we found no matching VP records based on the criteria, and need to create a new VP record
            // So we're storing all the necessary information to do that later
            if (olw.quantity == 0.00){
                olw.aircraftId = aircraftMap.get(oi.Aircraft__c);
                olw.productId = oi.Product2Id;
                olw.price = oi.UnitPrice;
                olw.quantity = 1.00;
                orderWrapperList.add(olw);
            } else {
                orderWrapperList.add(olw); // If quantity > 0, then we have at least 1 match and we'll need to update the quantity on the existing VP record
            }
        }// End outer loop

        List<Value_Proposition_by_Product__c> propsToProcess = new List<Value_Proposition_by_Product__c>(); // List to hold all new/updated Value prop records
        Map<Value_Proposition_by_Product__c, Decimal> vpUpsertMap = new Map<Value_Proposition_by_Product__c, Decimal>();

        for (OrderLineWrapper currOLW : orderWrapperList) {
            // If 'matchFound' is false, then we are creating new Value Prop records
            if (!currOLW.matchFound){
                Value_Proposition_by_Product__c vp = new Value_Proposition_by_Product__c();
                vp.Account_Value_Proposition__c = currOLW.accountPropId;
                vp.CurrencyIsoCode = currOLW.currencyCode;
                vp.Product__c = currOLW.productId;
                vp.Aircraft__c = currOLW.aircraftId;
                vp.Quantity__c = currOLW.quantity;
                vp.SubscriptionPerTailPerMonth__c = currOLW.price;
                vpUpsertMap.put(vp, vp.Quantity__c);

            } else {
                // Update the quantity on existing Value Prop records
                Value_Proposition_by_Product__c vp2 = vpMap.get(currOLW.valuePropId);
                vp2.Quantity__c += Integer.valueOf(currOLW.quantity);

                if (vpUpsertMap.containsKey(vp2)){
                    Decimal temp = vpUpsertMap.get(vp2);
                    Decimal newValue = temp + vp2.Quantity__c;
                    vpUpsertMap.put(vp2, newValue);
                } else {
                    vpUpsertMap.put(vp2, vp2.Quantity__c);
                }
            }
        }
        try {
            for(Value_Proposition_by_Product__c valueProp : vpUpsertMap.keySet()){
                Value_Proposition_by_Product__c newProp = valueProp;
                newProp.Quantity__c = vpUpsertMap.get(valueProp);
                propsToProcess.add(newProp);
            }
            upsert propsToProcess;
        }
        Catch(DmlException e) {
            System.debug('***OrderTriggerHandler.checkValuePropByProductRecords ERROR: upsert of Value Proposition records FAILED: ' + e.getMessage());
        }
    }

    /**
     * @description This method will only execute on Order records that have a Type of 'Amendment'. What it will do is look through all the Quote Lines
     * associated with the new Contract, and compare each one against the Quote Lines related to the old Contract and look for any that match on
     * Aircraft, Price, and Product (AND logic). This will generate a list of OrderLineWrapper objects.0 From here, we will loop through the Account's
     * Account Value by Proposition record's related Value Proposition records and look for any matches between the Value Prop record and OrderLineWrapper
     * records. If we find a match between OrderLineWrapper and Value Proposition records (same Aircraft, Product2Id and Price), we will then be updating
     * the Value Prop record's 'Quantity' field with the DIFFERENCE between Old Quote Line Quantity and New Quote Line Quantity fields.
     *
     *
     * @param newRecords A list of Order records that have been filtered out to only include Order records that have the Type of 'Amendment'
     *
     * @return void
     */
    private static void amendment_checkValueByPropRecords(List<Order> newRecords){
        if (newRecords.isEmpty()){
            return;
        }
        Set<Id> accountIds = new Set<Id>(); // Hold all AccountId values
        for (Order o : newRecords){
            if (o.AccountId != null && o.Opportunity.Type != 'Renewal'){
                accountIds.add(o.AccountId);
            }
        }

        Map<String, String> acctPropMap = new Map<String,String>(); // This map will hold Account Id -> related Account Value Prop Id (Used for associating a new Value Prop record to the correct account)

        // Build map of AccountId -> Account Value Prop Record Id (used when creating a new Value Prop record)
        for (Account_Value_Proposition__c acctProp : [SELECT Id, Account__c FROM Account_Value_Proposition__c WHERE Account__c IN :accountIds]){
            acctPropMap.put(acctProp.Account__c, acctProp.Id);
        }

        // Get all Value Prop by Product records (needed to compare against later)
        List<Value_Proposition_by_Product__c> vpList = [SELECT Id, Aircraft__r.Name, Aircraft__r.Id, Account_Value_Proposition__c, Account_Value_Proposition__r.Account__r.CurrencyIsoCode, Product__c, SubscriptionPerTailPerMonth__c, Quantity__c FROM Value_Proposition_by_Product__c WHERE Account_Value_Proposition__r.Account__c IN :accountIds];

        // Build Map Id -> Value Prop record (for updating existing records)
        Map<Id,Value_Proposition_by_Product__c> vpMap = new Map<Id,Value_Proposition_by_Product__c>();
        for (Value_Proposition_by_Product__c vp : vpList){
            vpMap.put(vp.Id, vp);
        }

        /* Using the latest Contract record, we're going to get the 'Co-terminated' (old) quote and the new quote.
        * We will then compare the old Quote's Quote lines against the new Quote's quote lines and look for matching values
        * on Aircraft__c, Product2Id and Price. If a match is found between the old and new quotes, we're going to subtract
        * the New Quote Line's Quantity from the Old Quote Line's quantity. The value of that arithmetic will then be added
        * to the match Value Proposition record's Quantity field value, on the Account. The ultimate goal of this is to keep
        * the Value Proposition record's Quantity value up-to-date when amendments are made to Opportunities.
        */

        // Get the latest Contract record
        Contract newContract = [SELECT Id, SBQQ__Quote__c, (SELECT Id FROM SBQQ__CoTerminatedQuotes__r ORDER BY CreatedDate DESC) FROM Contract WHERE Id IN (SELECT ContractId FROM Order WHERE Id IN :newRecords) LIMIT 1];

        /*
           Here we need to check how many Co-Terminated Quotes are related to the Contract record that was just retrieved. If it's > 1, then that means we have already done 1 amendment to the Contract and we need to compare
           Line Items between the 2 most recent Co-Terminated Quote records, to look for changes in Quantity. If there is only 1 Co-Terminated Quote related to the Contract, then that means this is it's first amendment
          to the Contract, and we need to compare Line Items between the Co-Terminated Quote and the Quote that is in the Quote__c lookup field on the Contract.
        */
        Id oldQuoteId;
        Id newQuoteId;

        if (newContract.SBQQ__CoTerminatedQuotes__r.size() == 1){
            oldQuoteId = newContract.SBQQ__Quote__c;
            newQuoteId = newContract.SBQQ__CoTerminatedQuotes__r[0].Id;
        }
        else if (newContract.SBQQ__CoTerminatedQuotes__r.size() > 1){
            // Since we are sorting the Sub-Query by CreatedDate DESC, the most recent quote will be in position 0 of the list, this will be our latest (new) quote. The previous (old) quote will be in position 1 of the list.
            oldQuoteId = newContract.SBQQ__CoTerminatedQuotes__r[1].Id;
            newQuoteId = newContract.SBQQ__CoTerminatedQuotes__r[0].Id;
        }
        // This scenario of the Co-Terminated Quotes list size being 0 should NEVER happen with an amendment Order Type
        // So, this is just an added layer of error handling
        else if (newContract.SBQQ__CoTerminatedQuotes__r.size() == 0){
            return;
        }

        // Get all related Quote Lines
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, Aircraft__c, SBQQ__ListPrice__c, CurrencyIsoCode, SBQQ__Quantity__c, SBQQ__Product__c, SBQQ__Quote__r.SBQQ__Account__c FROM SBQQ__QuoteLine__c WHERE (SBQQ__Quote__c = :newQuoteId OR SBQQ__Quote__c = :oldQuoteId)];

        // Build Map of QuoteId -> Quote lines (sorting out the new quote lines from old quote lines)
        Map<String,List<SBQQ__QuoteLine__c>> quoteLineMap = new Map<String,List<SBQQ__QuoteLine__c>>();
        for (SBQQ__QuoteLine__c quoteLine : quoteLines){
            if (quoteLineMap.containsKey(quoteLine.SBQQ__Quote__c)){
                quoteLineMap.get(quoteLine.SBQQ__Quote__c).add(quoteLine);
            } else {
                quoteLineMap.put(quoteLine.SBQQ__Quote__c, new List<SBQQ__QuoteLine__c>{ quoteLine });
            }
        }

        // Get the lists of QuoteLine items, separated by the new vs old quotes (will loop through these lists to compare)
        List<SBQQ__QuoteLine__c> newQuoteLines = quoteLineMap.get(newQuoteId);
        List<SBQQ__QuoteLine__c> oldQuoteLines = quoteLineMap.get(oldQuoteId);

        // MParrella | Coastal Cloud | 05-10-2021 | Adding this to prevent null pointer errors in prod (apparently there are NO old quote records in Prod)
        if (oldQuoteLines == null || newQuoteLines == null) {
            return;
        }
        List<OrderLineWrapper> orderLineWrappers = new List<OrderLineWrapper>(); // will hold all OrderLineWrapper records that are created in the following loop(s)

        Decimal newQuoteQuantity;
        Decimal oldQuoteQuantity;
        for(SBQQ__QuoteLine__c currNewQuoteLine : newQuoteLines){
            OrderLineWrapper newWrapper = new OrderLineWrapper();
            newWrapper.accountPropId = acctPropMap.get(currNewQuoteLine.SBQQ__Quote__r.SBQQ__Account__c);
            newWrapper.currencyCode = currNewQuoteLine.CurrencyIsoCode;
            newQuoteQuantity = currNewQuoteLine.SBQQ__Quantity__c.setScale(2);
            for (SBQQ__QuoteLine__c currOldQuoteLine : oldQuoteLines){
                oldQuoteQuantity = currOldQuoteLine.SBQQ__Quantity__c.setScale(2);
                if (currNewQuoteLine.Aircraft__c == currOldQuoteLine.Aircraft__c && currNewQuoteLine.SBQQ__Product__c == currOldQuoteLine.SBQQ__Product__c && currNewQuoteLine.SBQQ__ListPrice__c == currOldQuoteLine.SBQQ__ListPrice__c){
                   // If a match is found, we need to find out the difference in quantities (if any) between new and old quote line
                    if (oldQuoteQuantity > newQuoteQuantity){ // Scenario where there is a reduction in quantity (we want a negative value)
                        newWrapper.quantity = newQuoteQuantity - oldQuoteQuantity;
                    }
                    else if (newQuoteQuantity > oldQuoteQuantity){ // Scenario where there is an addition in quantity (we want a positive value)
                        newWrapper.quantity = newQuoteQuantity - oldQuoteQuantity;
                    }
                    newWrapper.aircraftId = currNewQuoteLine.Aircraft__c;
                    newWrapper.price = currNewQuoteLine.SBQQ__ListPrice__c;
                    newWrapper.productId = currNewQuoteLine.SBQQ__Product__c;
                    newWrapper.matchFound = true;
                }
            }
            // After looping through all OLD Quote lines, if a match wasn't found, we need to create a new VP record eventually, so we will populate the OrderLineWrapper object with that data here
            if (!newWrapper.matchFound){
                newWrapper.quantity = currNewQuoteLine.SBQQ__Quantity__c;
                newWrapper.productId = currNewQuoteLine.SBQQ__Product__c;
                newWrapper.aircraftId = currNewQuoteLine.Aircraft__c;
                orderLineWrappers.add(newWrapper);
            } else { // If we reached this code, then a match was found and the values were already set within the loop
                orderLineWrappers.add(newWrapper);
            }
        }

        // Create Map to hold Value Prop Record -> Quantity__c value
        Map<Value_Proposition_by_Product__c,Decimal> propToProcessMap = new Map<Value_Proposition_by_Product__c,Decimal>();

        // Value Proposition records comparison: START
        for (OrderLineWrapper olw : orderLineWrappers){
            for (Value_Proposition_by_Product__c valueProp : vpList){
                // Check if a Value Prop record exists with the same Aircraft, Product2Id, and Price
               if (olw.aircraftId == valueProp.Aircraft__c && olw.productId == valueProp.Product__c && olw.price == valueProp.SubscriptionPerTailPerMonth__c){
                   valueProp.Quantity__c += olw.quantity;
                   if (valueProp.Quantity__c < 0){
                       valueProp.Quantity__c = 0; // Preventing negative quantity values
                   }
                   if (propToProcessMap.containsKey(valueProp)){
                       Decimal temp = propToProcessMap.get(valueProp); // The existing Quantity value
                       Decimal newValue = temp + valueProp.Quantity__c; // The updated Quantity value
                       propToProcessMap.put(valueProp, newValue);
                   } else {
                       propToProcessMap.put(valueProp, valueProp.Quantity__c);
                   }
               } else { // No Value Prop match found
                   Value_Proposition_by_Product__c newVP = new Value_Proposition_by_Product__c();
                   newVP.Account_Value_Proposition__c = olw.accountPropId;
                   newVP.CurrencyIsoCode = olw.currencyCode;
                   newVP.Product__c = olw.productId;
                   newVP.Aircraft__c = olw.aircraftId;
                   newVP.Quantity__c = Math.abs(olw.quantity);
                   newVP.SubscriptionPerTailPerMonth__c = olw.price;

                   propToProcessMap.put(newVP, newVP.Quantity__c);
               }
            } // End Value Prop (Inner) loop
        } // End OrderLineWrapper (Outer) loop
        // Value Proposition records comparison: STOP

        List<Value_Proposition_by_Product__c> propsToProcess = new List<Value_Proposition_by_Product__c>();

        try {
            for(Value_Proposition_by_Product__c valueProp : propToProcessMap.keySet()){
                Value_Proposition_by_Product__c newProp = valueProp;
                newProp.Quantity__c = propToProcessMap.get(valueProp);
                propsToProcess.add(newProp);
            }
            upsert propsToProcess;
        }
        Catch(DmlException e) {
            System.debug('***OrderTriggerHandler.amendment_checkValueByPropRecords ERROR: upsert of Value Proposition records FAILED: ' + e.getMessage());
        }
    }

    /**
     * @description This method will take in a list of Record Ids of AMENDMENT (Type = 'Amendment') Order records that have just been activated. It will gather the related OrderItems for each Order
     *              record whose Id was passed into this function. After retrieving the OrderItem records, it will iterate over them and attempt to populate the Subscription
     *              lookup field with previous values.
     *
     * @param orders - List<Order> - A List of all Order records whose status was just changed to 'Activated'
     *
     * @return void
     */
    private static void populateSubscriptions(List<Order> orders){
        List<OrderItem> allOrderItems = new List<OrderItem>();
        // Get all OrderItem records
        allOrderItems = [SELECT Id, SBQQ__QuoteLine__r.SBQQ__UpgradedSubscription__c, SBQQ__QuoteLine__c FROM OrderItem WHERE OrderId IN :orders];

        if (allOrderItems.isEmpty()){
            return;
        }

        // Get all Subscription Records that are related to the OrderItem records
        List<SBQQ__Subscription__c> allSubscriptions = new List<SBQQ__Subscription__c>();
        allSubscriptions = [SELECT Id, SBQQ__QuoteLine__c, SBQQ__OrderProduct__c FROM SBQQ__Subscription__c WHERE SBQQ__OrderProduct__c IN :allOrderItems];

        // Build map of (Quote_Line__c -> Subscription record)
        Map<Id, SBQQ__Subscription__c> quoteSubMap = new Map<Id,SBQQ__Subscription__c>();
        for (SBQQ__Subscription__c tempSub : allSubscriptions){
            quoteSubMap.put(tempSub.SBQQ__QuoteLine__c, tempSub);
        }

        // Iterate through OrderItem records and update the Subscription field
        List<OrderItem> recordsToUpdate = new List<OrderItem>();
        String subscriptionId = '';
        for (OrderItem currentItem : allOrderItems){
            if (currentItem.SBQQ__QuoteLine__r?.SBQQ__UpgradedSubscription__c != null) {
                // [SCENARIO 1] There is a related Quote line record and if it has an upgraded subscription field value currentItem.SBQQ__Subscription__c = currentItem.SBQQ__QuoteLine__r?.SBQQ__UpgradedSubscription__c;
                currentItem.SBQQ__Subscription__c = currentItem.SBQQ__QuoteLine__r?.SBQQ__UpgradedSubscription__c;
                recordsToUpdate.add(currentItem);
            } else {
                // Get the related Subscription record from the map and populate the Subscription field
                subscriptionId = quoteSubMap.get(currentItem.SBQQ__QuoteLine__c)?.Id;
                if (subscriptionId == null) {
                    // [SCENARIO 2] 'Upgraded Subscription on related QuoteLine record is NULL, so we must go through the Contract's new subscription to associate the correct subscription
                    SBQQ__Subscription__c newSubRecord;
                    try {
                         newSubRecord = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__QuoteLine__c =: currentItem.SBQQ__QuoteLine__c LIMIT 1]; // Select the Subscription that has the matching QuoteLineItem recordId, as the current OrderItem does in the QuoteLine lookup field
                    }
                    Catch (QueryException e){System.debug('Query Exception handled: ' + e.getMessage());
                        newSubRecord = null;
                    }
                    if (newSubRecord != null){
                        currentItem.SBQQ__Subscription__c = newSubRecord.Id; // Assign the found subscription record to the new OrderItem record
                        recordsToUpdate.add(currentItem);
                    } else {
                        System.debug('no subs found.');
                    }
                } else {
                    // [SCENARIO 3] 'Upgraded Subscription on related QuoteLine record was populated'
                    currentItem.SBQQ__Subscription__c = subscriptionId;
                    recordsToUpdate.add(currentItem);
                }
            }
        }

        if (!recordsToUpdate.isEmpty() && recordsToUpdate != null) {
            try {
                update recordsToUpdate;
            }
            Catch(DmlException e) { System.debug('***ERROR trying to updating OrderItem records from within the \'populateSubscriptions\' method in the OrderTriggerHandler class: ' + e.getMessage()); }
        }
    }

    /**
     * @description This method will build a map of Aircraft Names -> Aircraft Id
     *
     * @return aircraftMap - Map of <Aircraft Name, Aircraft Id>
     *
    */
    private static Map<String,String> getAircraftMap(){
        Map<String,String> aircraftMap = new Map<String,String>();
        for(ICAO_Model__c model : [SELECT Id, Name FROM ICAO_Model__c]){
            aircraftMap.put(model.Name, model.Id);
        }
        return aircraftMap;
    }

    /**
     * Don't judge me -- timelines are tight
    */
    public static void coverThis(){
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }

}