/**
 *
 * Original - Coastal Cloud - Matt Parrella - 03/18/2021 - Creating initial version of test class to cover 'LeadTriggerHandler' and 'LeadTrigger' Apex classes
 *
 */
@IsTest
public with sharing class LeadTriggerHandler_Test {
    @TestSetup
    static void setup_data(){
        // Create Lead
        Lead testLead = new Lead();
        testLead.FirstName = 'Ricky';
        testLead.LastName = 'Bobby';
        testLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Person_Account_Lead').getRecordTypeId();

        insert testLead;

        // Create CPQ Subscription records
        List<SBQQ__Subscription__c> subs = new List<SBQQ__Subscription__c>();
        for (Integer i = 0; i < 5; i++){
            subs.add(new SBQQ__Subscription__c(
                    SBQQ__ProductSubscriptionType__c = 'Trial',
                    SBQQ__SubscriptionType__c = 'Trial',
                    SBQQ__SubscriptionStartDate__c = Date.today(),
                    SBQQ__SubscriptionEndDate__c = Date.today().addDays(90),
                    SBQQ__Quantity__c = 1,
                    Lead__c = testLead.Id
            ));
        }
        insert subs;
    }

    @IsTest
    static void testAutoConverted(){
        // Get the test Lead record
        Lead testLead = [SELECT Id, Name, (SELECT Id FROM Subscriptions__r) FROM Lead WHERE Name = 'Ricky Bobby' LIMIT 1];
        System.assertEquals(5, testLead.Subscriptions__r.size(), 'Lead should have 5 related Subscription records.'); // Assert all the subscription records are there

        // Convert the lead
        Test.startTest();
        testLead.Auto_Converted__c = True;
        update testLead;
        Test.stopTest();

        // Assert new Account's RecordType and that Subscriptions were re-parented
        Account newAccount = [SELECT Id, RecordType.DeveloperName, (SELECT Id FROM SBQQ__Subscriptions__r) FROM Account WHERE Name = 'Ricky Bobby' LIMIT 1];
        System.assertEquals('PersonAccount', newAccount.RecordType.DeveloperName, 'Should be a Person Account RT.');
        System.assertEquals(5, newAccount.SBQQ__Subscriptions__r.size(), 'Should have 5 related subscriptions which came over from the Lead during conversion.');
        System.assertEquals('Qualified', [SELECT Status FROM Lead WHERE Id = :testLead.Id].Status, 'Lead status should be \'Qualified\' after conversion.');

    }
}