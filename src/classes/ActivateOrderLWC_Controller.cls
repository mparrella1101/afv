/**
 * Original - Coastal Cloud - Matt Parrella - 03/19/2021 - Creating initial version of Apex Controller for the activateOrder LWCs
 */

public with sharing class ActivateOrderLWC_Controller {

    /**
     * Wrapper class used to represent Production_System__c records
     */
    public class ProdSystemWrapper {
        @AuraEnabled public String name;
        @AuraEnabled public String id;
        @AuraEnabled public String systemName;
        @AuraEnabled public String accountId;
        @AuraEnabled public List<SystemTailWrapper> systemTailList;
        public ProdSystemWrapper(){
            this.name = '';
            this.systemName = '';
            this.accountId = '';
            this.systemTailList = new List<SystemTailWrapper>();
        }
    }

    /**
     * Wrapper class used to represent System_Tail__c records (used for populating Picklist input)
     */

    public class SystemTailWrapper {
        @AuraEnabled public String value;
        @AuraEnabled public String label;
        public SystemTailWrapper(){ }
        public SystemTailWrapper(String inputVal, String inputLabel){
            this.value = inputVal;
            this.label = inputLabel;
        }
    }

    /**
     * Wrapper class used to represent a DML Operation result
     */

    public class ResponseWrapper{
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public String errorMsg;
        public ResponseWrapper(){
            this.isSuccess = true;
            this.errorMsg = '';
        }
    }

    /**
     * Wrapper class used to represent an OrderItem record
     */

    public class OrderLineItemWrapper{
        @AuraEnabled public String name;
        @AuraEnabled public String id;
        @AuraEnabled public String aircraft;
        @AuraEnabled public String productId;
        @AuraEnabled public String systemTailName;
        @AuraEnabled public String systemTailId;
        @AuraEnabled public String recordId;
        @AuraEnabled public Boolean isNew;
        @AuraEnabled public String productName;
        @AuraEnabled public String systemName;
        @AuraEnabled public String systemId;
        @AuraEnabled public String orderId;
        @AuraEnabled public Double unitPrice;
        @AuraEnabled public Integer numMatches;
        @AuraEnabled public Boolean hasMatchingProdSys;
        @AuraEnabled public String missingSystemName;
        @AuraEnabled public String selectedTailId;
        @AuraEnabled public String tailId;
        @AuraEnabled public String changeType; // Used when Quote type = 'amendment'
        @AuraEnabled public List<SystemTailWrapper> availableTails;
        public OrderLineItemWrapper(){
            this.selectedTailId = '';
            this.numMatches = 0;
            this.tailId = '';
            this.availableTails = new List<SystemTailWrapper>{ new SystemTailWrapper('','Select a Tail...') };
        }
    }

    /**
     * Wrapper class used to represent an OrderItem record when performing the update after System_Tail__c records have been
     * selected and associated with an OrderItem record.
     */
    public class OrderItemUpdate{
        public String recordId; // OrderItem record Id
        public String tailId; // Selected System_Tail__c record
        public String oldOrderId; // Old Order Record Id that is to be deactivated
    }

    /**
     * This method will retrieve the Production_System__c records related to the Account on the Order record. These returned values are
     * used to check if the user is trying to register System Tails on a System where there is no corresponding Production_System__c record
     * related to the Account (ex: OrderLine item's product family is 'APG', and there is no 'APG' Production_System__c record related to the account).
     *
     * @param accountId The Id of the Account associated with the Order record
     *
     * @return A list of 'ProdSystemWrapper' objects representing all the related Production_System__c records on the Account
     */

    @AuraEnabled
    public static List<ProdSystemWrapper> getProductionSystemRecords(String accountId){
        List<ProdSystemWrapper> prodSystemWrapperList = new List<ProdSystemWrapper>();
        Map<String, ProdSystemWrapper> prodSysMap = new Map<String, ProdSystemWrapper>();
        if (accountId == null || accountId == ''){
            return null;
        } else {
            // Loop through Prod Systems and populate map
            for(Production_System__c prodSys : getProductionSystems(accountId)){
                ProdSystemWrapper newProdWrapper = new ProdSystemWrapper();
                newProdWrapper.id = prodSys.id;
                newProdWrapper.name = prodSys.Name;
                newProdWrapper.systemName = prodSys.System__c;
                newProdWrapper.accountId = prodSys.Account__c;
                prodSysMap.put(newProdWrapper.id, newProdWrapper);
            }
            return prodSysMap.values();
        }
    }

    /**
     * This method is used to retrieve all OrderItem records related to the Order. Once obtained, the records are iterated
     * upon, to gather all the related System__c values. If the related Account does NOT have a matching Production_System__c record
     * for each OrderItem's Product Family, we populated the 'systemId' field with 'MISSING', so that we can determine which Systems
     * to alert the User with. (ex: OrderItem's System__c = 'RocketRoute' but there is no 'RocketRoute' Production_System__c record
     * on the Account, then we would mark this system as 'MISSING' to be displayed in an error message on the front-end)
     *
     * @param orderId
     * @param accountId
     *
     * @return A list of 'OrderLineItemWrapper' objects that represent OrderItem records
     */

    @AuraEnabled
    public static List<OrderLineItemWrapper> getOrderLineData(String orderId, String accountId){
        if (orderId == '' || orderId == null || accountId == '' || accountId == null){
            return null;
        }
        List<OrderLineItemWrapper> oliWrapperList = new List<OrderLineItemWrapper>();

        //Build list of Production System records associated with Account
        Map<String,Id> prodSystemsMap = new Map<String,Id>();
        for(Production_System__c prodSys : getProductionSystems(accountId)){
            prodSystemsMap.put(prodSys.System__c, prodSys.Id);
        }

        // Get OrderLineItem records and build list of OrderLineItemWrapper objects
        for(OrderItem oi : [SELECT Id, OrderId, Order_Line_Name__c, System__c, System_Tail__r.Name, Product2Id, Product2.Name, Aircraft__c, System_Tail__c FROM OrderItem WHERE OrderId =: orderId]){
            OrderLineItemWrapper oliWrapper = new OrderLineItemWrapper();
            oliWrapper.id = oi.id;
            oliWrapper.productId = oi.Product2Id;
            oliWrapper.productName = oi.Product2.Name;
            oliWrapper.name = oi.Order_Line_Name__c;
            oliWrapper.aircraft = oi.Aircraft__c;
            oliWrapper.systemTailName = oi.System_Tail__r?.Name != null ? oi.System_Tail__r.Name : '';
            oliWrapper.systemName = oi.System__c;
            if (prodSystemsMap.containsKey(oi.System__c)){
                oliWrapper.systemId = prodSystemsMap.get(oi.System__c);
            } else {
                oliWrapper.systemId = 'MISSING ' + oi.System__c;
            }
            oliWrapper.hasMatchingProdSys = prodSystemsMap.containsKey(oi.System__c) ? true : false;
            if (!oliWrapper.hasMatchingProdSys){
                oliWrapper.missingSystemName = oi.System__c;
            } else {
                oliWrapper.missingSystemName = '';
            }
            oliWrapperList.add(oliWrapper);
        }

        return oliWrapperList;
    }

    /**
     * This method is very similar to the 'getOrderLineData' method that is also within this class. The main difference between the two
     * is that this method will build a list of available System_Tail__c records for each OrderItem record returned from the query. This
     * list of System_Tail__c records is used in the datatable LWC so the user can select which System_Tail__C record to associate with an
     * OrderItem record.
     *
     * @param orderId The Id of the Order Record that launched the LWC
     * @param accountId The Id of the related Account on the Order Record that launched the LWC
     * @param prodSystems List of selected Systems by the user on the 'activateOrderSelectProdSys' LWC
     *
     * @return A list of 'OrderLineItemWrapper' objects that represent OrderItem records (with a list of available System_Tail__c records)
     */

    @AuraEnabled
    public static List<OrderLineItemWrapper> getSpecificOrderLineData(String orderId, String accountId, List<String> prodSystems){
        if (orderId == '' || orderId == null || accountId == '' || accountId == null){
            return null;
        }
        List<OrderLineItemWrapper> oliWrapperList = new List<OrderLineItemWrapper>();

        //Build list of Production System records associated with Account
        Map<String,Id> prodSystemsMap = new Map<String,Id>();
        for(Production_System__c prodSys : getProductionSystems(accountId)){
            prodSystemsMap.put(prodSys.System__c, prodSys.Id);
        }

        // Get OrderLineItem records and build list of OrderLineItemWrapper objects
        for(OrderItem oi : [SELECT Id, OrderId, Order_Line_Name__c, System__c, Product2Id, Product2.Name, Aircraft__c, System_Tail__c FROM OrderItem WHERE OrderId =: orderId AND System__c IN :prodSystems AND System_Tail__c = NULL]){
            OrderLineItemWrapper oliWrapper = new OrderLineItemWrapper();
            oliWrapper.id = oi.Id;
            oliWrapper.productId = oi.Product2Id;
            oliWrapper.productName = oi.Product2.Name;
            oliWrapper.name = oi.Order_Line_Name__c;
            oliWrapper.aircraft = oi.Aircraft__c;
            oliWrapper.orderId = oi.OrderId;
            oliWrapper.systemTailName = '';
            oliWrapper.systemName = oi.System__c;
            if (prodSystemsMap.containsKey(oi.System__c)){
                oliWrapper.systemId = prodSystemsMap.get(oi.System__c);
            } else {
                oliWrapper.systemId = 'MISSING ' + oi.System__c;
            }
            oliWrapper.hasMatchingProdSys = prodSystemsMap.containsKey(oi.System__c) ? true : false;
            if (!oliWrapper.hasMatchingProdSys){
                oliWrapper.missingSystemName = oi.System__c;
            } else {
                oliWrapper.missingSystemName = '';
            }
            oliWrapperList.add(oliWrapper);
        }
        Map<String,Map<String,List<SystemTailWrapper>>> systemTailMap = buildSystemTailLists(orderId,prodSystems,accountId);

        List<OrderLineItemWrapper> updatedList = new List<OrderLineItemWrapper>();
        for (OrderLineItemWrapper oli : oliWrapperList){
            if (systemTailMap.containsKey(oli.systemName)) {
                if (systemTailMap.get(oli.systemName).containsKey(oli.aircraft)){
                    oli.availableTails.addAll( systemTailMap.get(oli.systemName).get(oli.aircraft));
                    updatedList.add(oli);
                }
            }
        }
        return updatedList;
    }

    /**
     * This is a helper method that is used to determine which System_Tail__c records are available by sorting them in 2 ways: first they
     * are sorted by System (APG, RocketRoute, Seattle Avionics), then further by Aircraft. These lists are then used to populate the list of
     * available System Tails within the 'getSpecificOrderLineData' method within this class.
     *
     * @param orderId The Id of the Order record that launched the LWC
     * @param selectedSystems The list of selected systems from the User
     * @param accountId The Id of the Account record associated to the Order record that launched the LWC
     *
     * @return Returns a map of a map. Outer map's key values are the System Names (APG, RocketRoute, etc.) and the inner map's key values
     *         are the Aircraft names. Each Aircraft name is tied to a list of available System Tail records
     */
    @AuraEnabled
    public static Map<String,Map<String,List<SystemTailWrapper>>> buildSystemTailLists(String orderId, List<String> selectedSystems, String accountId){

        List<OrderItem> orderItems = [SELECT Id, Aircraft__c, System__c FROM OrderItem WHERE OrderId =: orderId AND System__c IN :selectedSystems];
        Set<Id> prodSystemIds = new Set<Id>();

        for (Production_System__c ps : getProductionSystems(accountId)){
            prodSystemIds.add(ps.Id);
        }

        // Build list of Production Systems and Aircrafts contained within OrderItems
        List<String> aircraftsInUse = new List<String>();
        for (OrderItem oi : orderItems){
            if (!aircraftsInUse.contains(oi.Aircraft__c)){
                aircraftsInUse.add(oi.Aircraft__c);
            }
        }
        Map<String,Map<String,List<SystemTailWrapper>>> ultraMap = new Map<String,Map<String,List<SystemTailWrapper>>>();
        /* ultraMap example to show data relationship:
         APG ->         aircraft1 -> available tail 1
                                     available tail 2
                        aircraft2 -> available tail 3
                                     available tail 4

         RocketRoute -> aircraft3 -> available tail 5
                                     available tail 6
                                     available tail 7
        */

        // Query for all available system tails
        List<System_Tail__c> allSystemTails = [SELECT Id, Aircraft__c, System__c, Tail_Aircraft_Name__c, Name FROM System_Tail__c WHERE Aircraft__c IN :aircraftsInUse AND Production_System__c IN :prodSystemIds AND Active__c = TRUE];

        // Iterate through tails and build 'ultraMap'
        for (System_Tail__c sysTail : allSystemTails){
            SystemTailWrapper newTailWrapper = new SystemTailWrapper();
            newTailWrapper.value = sysTail.Id;
            newTailWrapper.label = sysTail.Name;
            switch on sysTail.System__c {
                when 'APG'{
                    if (!ultraMap.containsKey('APG')) {
                        ultraMap.put('APG', new Map<String,List<SystemTailWrapper>>());
                    }
                    if (!ultraMap.get('APG').containsKey(sysTail.Aircraft__c)){
                        ultraMap.get('APG').put(sysTail.Aircraft__c, new List<SystemTailWrapper> { newTailWrapper });
                    } else {
                        ultraMap.get('APG').get(sysTail.Aircraft__c).add(newTailWrapper);
                    }
                }
                when 'RocketRoute' {
                    if (!ultraMap.containsKey('RocketRoute')) {
                        ultraMap.put('RocketRoute', new Map<String,List<SystemTailWrapper>>());
                    }
                    if (!ultraMap.get('RocketRoute').containsKey(sysTail.Aircraft__c)){
                        ultraMap.get('RocketRoute').put(sysTail.Aircraft__c, new List<SystemTailWrapper> { newTailWrapper });
                    } else {
                        ultraMap.get('RocketRoute').get(sysTail.Aircraft__c).add(newTailWrapper);
                    }
                }
                when 'Seattle Avionics' {
                    if (!ultraMap.containsKey('Seattle Avionics')) {
                        ultraMap.put('Seattle Avionics', new Map<String,List<SystemTailWrapper>>());
                    }
                    if (!ultraMap.get('Seattle Avionics').containsKey(sysTail.Aircraft__c)){
                        ultraMap.get('Seattle Avionics').put(sysTail.Aircraft__c, new List<SystemTailWrapper> { newTailWrapper });
                    } else {
                        ultraMap.get('Seattle Avionics').get(sysTail.Aircraft__c).add(newTailWrapper);
                    }
                }
                when else {

                }
            }
        }
        return ultraMap;
    }

    /**
     * Helper method that retrieves all Production_System__c records related to a given Account whose Id is passed-in
     * to the method.
     *
     * @param accountId Id of Account record for which you want the related Production_System__c records
     *
     * @return A list of Production_System__c records
     */
    public static List<Production_System__c> getProductionSystems(String accountId){
        return [SELECT Id, System__c, Account__c, Name FROM Production_System__c WHERE Account__c =: accountId];
    }

    /**
     * This method is responsible for updating the OrderItem records after System_Tail__c records have been assigned to them.
     *
     * @param inputVal The JSON object passed-in from the Front-End, containing OrderItem record updates
     *
     * @return A ResponseWrapper object indicating if there were any errors encountered when updating the records
     */

    @AuraEnabled
    public static ResponseWrapper updateOrderLineItems(String inputVal){
        ResponseWrapper resp = new ResponseWrapper();

        // Create list of Order Product records to update
        List<OrderItem> orderItemsToUpdate = new List<OrderItem>();

        // Parse the passed-in list of JS Objects (deserialize and put in list)
        List<OrderItemUpdate> inputParse = (List<OrderItemUpdate>) System.JSON.deserialize(inputVal, List<OrderItemUpdate>.class);

        // Create OrderItem records to update
        for (OrderItemUpdate oiu : inputParse){
            orderItemsToUpdate.add(new OrderItem(
                    id = oiu.recordId,
                    System_Tail__c = oiu.tailId
            ));
        }
        try{
            update orderItemsToUpdate;
            return resp;
        }
        Catch(DmlException e){
            resp.isSuccess = false;
            resp.errorMsg = e.getMessage();
            return resp;
        }
    }

    /**
     * This method is responsible for Activating both the Order and the related Contract records.
     *
     * @param orderId The Id of the Order record that is being Activated.
     *
     * @return A ResponseWrapper object indicating if there were any errors encountered during the activation of the Order and
     *         Contract records
     */

    @AuraEnabled
    public static ResponseWrapper activateRecords(String orderId){
        ResponseWrapper resp = new ResponseWrapper();
        Contract contractRecord;

        Order orderRecord = [SELECT Id, Status, SBQQ__Quote__r.SBQQ__MasterContract__c, ContractId FROM Order WHERE Id =: orderId];
        if (orderRecord.ContractId != null){
            contractRecord = [SELECT Id, Status FROM Contract WHERE Id =: orderRecord.ContractId];
        } else {
            if (orderRecord.SBQQ__Quote__r.SBQQ__MasterContract__c != NULL){
                contractRecord = [SELECT Id, Status FROM Contract WHERE Id =: orderRecord.SBQQ__Quote__r.SBQQ__MasterContract__c];
            }
        }

        if (contractRecord == null){
            resp.isSuccess = false;
            resp.errorMsg = 'Contract record not found.';
            return resp;
        } else {
            contractRecord.Status = 'Activated';
            orderRecord.Status = 'Activated';

            try {
                update contractRecord;
                update orderRecord;
                return resp;
            }
            Catch(DmlException e){
                System.debug('****' + e.getMessage());
                resp.isSuccess = false;
                resp.errorMsg = e.getMessage();
                return resp;
            }
        }
    }

    /**
     * A helper method that simply returns a Boolean TRUE value if the Status of the passed-in Order Id is 'Activated'. This is used
     * to deactivate the 'Activate' button on the LWC, if the Order record is already activated
     *
     * @param orderId The Id of the Order Record that is to be activated
     *
     * @return True - Order's Status = 'Activated', False - Order's Status != 'Activated'
     */

    @AuraEnabled
    public static Boolean checkActivated(String orderId){
        return [SELECT Status FROM Order WHERE Id =: orderId].Status == 'Activated';
    }

    /**
     * This metho will get both the old and new Order Item records to display to the end-user informing them that the old
     * Order Item records will have their System Tails deregistered.
     *
     * @param newOrderId The record Id of the Order that launched the LWC
     *
     * @return List<OrderLineItemWrapper> The list of all Order Items (from both Old and New Order records), denoted by use of
     *         the 'isNew' Attribute
     */
    @AuraEnabled
    public static List<OrderLineItemWrapper> getAllOrderItems(String orderId){
        List<OrderLineItemWrapper> fullList = new List<OrderLineItemWrapper>();
        fullList.addAll(getOldOrderItems(orderId));
        fullList.addAll(getNewOrderItems(orderId));

        return fullList;
    }

    /**
     * This helper method will retrieve OrderItem records from the OLD Order record, construct wrapper objects to represent the records, and finally
     * sends the collection of wrapper objects to the front end to display to the user.
     *
     * @param newOrderId The record Id of the new Order Id
     *
     * @return A collection of OrderLineItemWrapper objects, representing OrderItem records from the OLD Order record
     */

    public static List<OrderLineItemWrapper> getOldOrderItems(String newOrderId){
        // Get the contract Id and old Quote Id
        Order newOrderRecord = [SELECT ContractId, SBQQ__Quote__r.SBQQ__MasterContract__c, SBQQ__Quote__r.SBQQ__MasterContract__r.SBQQ__Quote__c FROM Order WHERE Id =: newOrderId];

        // MParrella | CoastalCloud | 05-10-2021 | Adding check for Originating Contract, if it isn't found use the old order Id to get old order Items
        if (newOrderRecord.SBQQ__Quote__r.SBQQ__MasterContract__r.SBQQ__Quote__c == null ){
            // Using the old OrderId
            // Get a Subscription record from the Contract, which we will use to query for the old OrderItem record
            SBQQ__Subscription__c tempSubRecord = [SELECT Id, Aircraft__r.Name, SBQQ__Contract__c FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: newOrderRecord.ContractId AND Id IN (SELECT SBQQ__Subscription__c FROM OrderItem) LIMIT 1];
            // Get the Old OrderId from the Subscription record
            String oldOrderId = [SELECT OrderId FROM OrderItem WHERE SBQQ__Subscription__c =: tempSubRecord.Id LIMIT 1].OrderId;
            List<OrderLineItemWrapper> oldOrderItemWrappers = new List<OrderLineItemWrapper>();
            for(OrderItem oi : [SELECT Id, Product2Id, Product2.Name, OrderId, Aircraft__c, UnitPrice, System_Tail__r.Name FROM OrderItem WHERE OrderId =: oldOrderId]){
                OrderLineItemWrapper wrapper = new OrderLineItemWrapper();
                wrapper.id = oi.id;
                wrapper.productId = oi.Product2Id;
                wrapper.recordId = oi.id;
                wrapper.isNew = false;
                wrapper.productName = oi.Product2.Name;
                wrapper.orderId = oi.OrderId;
                wrapper.systemTailName = oi.System_Tail__r.Name;
                wrapper.systemTailId = oi.System_Tail__c;
                wrapper.aircraft = oi.Aircraft__c;
                wrapper.unitPrice = oi.UnitPrice;
                oldOrderItemWrappers.add(wrapper);
            }
            System.debug('Old OrderItems W/O Orig. Quote: ' + oldOrderItemWrappers);
            return oldOrderItemWrappers;
        } else { // Originating Contract exists
            String contractId = newOrderRecord?.SBQQ__Quote__r?.SBQQ__MasterContract__c;
            String oldQuoteId = newOrderRecord?.SBQQ__Quote__r?.SBQQ__MasterContract__r?.SBQQ__Quote__c;
            if (contractId == null || contractId == '' || oldQuoteId == null || oldQuoteId == ''){
                return null;
            }

            // Using the old Quote Id, retrieve the old Order Item records and create a list of wrapper objects to send to the front-end
            List<OrderLineItemWrapper> oldOrderItemWrappers = new List<OrderLineItemWrapper>();
            for(OrderItem oi : [SELECT Id, Product2Id, Product2.Name, OrderId, Aircraft__c, UnitPrice, System_Tail__r.Name FROM OrderItem WHERE OrderId IN (SELECT Order__c FROM SBQQ__Quote__c WHERE Id =: oldQuoteId)]){
                OrderLineItemWrapper wrapper = new OrderLineItemWrapper();
                wrapper.id = oi.id;
                wrapper.productId = oi.Product2Id;
                wrapper.recordId = oi.id;
                wrapper.isNew = false;
                wrapper.productName = oi.Product2.Name;
                wrapper.orderId = oi.OrderId;
                wrapper.systemTailName = oi.System_Tail__r.Name;
                wrapper.systemTailId = oi.System_Tail__c;
                wrapper.aircraft = oi.Aircraft__c;
                wrapper.unitPrice = oi.UnitPrice;
                oldOrderItemWrappers.add(wrapper);
            }
            return oldOrderItemWrappers;
        }
    }

    /**
     * This helper method will retrieve OrderItem records from the NEW Order record, construct wrapper objects to represent the records, and finally
     * sends the collection of wrapper objects to the front end to display to the user.
     *
     * @param newOrderId The record Id of the new Order Id
     *
     * @return A collection of OrderLineItemWrapper objects, representing OrderItem records from the NEW Order record
     */

    public static List<OrderLineItemWrapper> getNewOrderItems(String newOrderId){

        // Query for OrderItem records related to the new Order
        List<OrderLineItemWrapper> newOrderItemWrappers = new List<OrderLineItemWrapper>();
        for(OrderItem oi : [SELECT Id, Product2Id, System_Tail__r.Name, System_Tail__c, OrderId, Product2.Name, UnitPrice, Aircraft__c FROM OrderItem WHERE OrderId =: newOrderId]){
            OrderLineItemWrapper wrapper = new OrderLineItemWrapper();
            wrapper.id = oi.id;
            wrapper.productId = oi.Product2Id;
            wrapper.recordId = oi.id;
            wrapper.productName = oi.Product2.Name;
            wrapper.isNew = true;
            wrapper.unitPrice = oi.UnitPrice;
            wrapper.orderId = oi.OrderId;
            wrapper.systemTailName = oi.System_Tail__r.Name;
            wrapper.systemTailId = oi.System_Tail__c;
            wrapper.aircraft = oi.Aircraft__c;
            newOrderItemWrappers.add(wrapper);
        }
        return newOrderItemWrappers;
    }

    /**
     * This method will take in an Order Record Id, retrieve all the related Order Item records and 'clear out' the value
     * in the System_Tail__c field (essentially de-registering Order Items)
     *
     * @param jsonInput OrderItem data in JSON format, passed to the back-end from the front-end
     * @param orderId The record Id of the Order for which Order Items will be registered
     *
     * @return A ResponseWrapper indicating if transaction was successful or not. Includes error message
     */
    @AuraEnabled
    public static ResponseWrapper deactivateOldOrderItems(String jsonInput, String oldOrderId){
        ResponseWrapper resp = new ResponseWrapper();

        // Deserialize the JSON input into OrderLineItem objects
        List<OrderItemUpdate> inputParse = (List<OrderItemUpdate>) System.JSON.deserialize(jsonInput, List<OrderItemUpdate>.class);


        // Build list of OrderItem record Ids (will be used to Query for Subscription records)
        Set<Id> orderItemIds = new Set<Id>();
        for(OrderItemUpdate oi : inputParse){
            orderItemIds.add(oi.recordId);
        }

        // Get OLD Order Record to deactivate
        Order oldOrder = [SELECT Id, Status FROM Order WHERE Id =: oldOrderId LIMIT 1];

        // Retrieve and iterate over the Old Order Item records, de-registering each System Tail
        List<OrderItem> itemsToUpdate = new List<OrderItem>();
        List<OrderItem> items = [SELECT Id, OrderId, SBQQ__Subscription__c, System_Tail__c FROM OrderItem WHERE Id IN :orderItemIds];
        for(OrderItem oi : items){
            System.debug(oi);
            oi.System_Tail__c = null;
            oi.SBQQ__Subscription__c = null;
            itemsToUpdate.add(oi);
        }

        try {
            update itemsToUpdate;
            oldOrder.Status = 'No Longer Active';
            update oldOrder;
            return resp;
        }
        Catch(DmlException e){
            resp.isSuccess = false;
            resp.errorMsg = e.getMessage();
            return resp;
        }
    }

    /**
     * A simple method to determine if a Contract record is related to the Order or not (since the Contract is created via CPQ in a future
     * method, we have no way of knowing when the Contract record will be created. In order to avoid the user registering tails and encountering an error
     * afterwards due to no contract being found, this method will execute upon component render and check immediately if there is a contract
     * or not.
     *
     * @param orderId The Record Id of the Order record that we're checking
     *
     * @return result The boolean representation of whether or not there is a related Contract on the Order record
     *
    */
    @AuraEnabled
    public static Boolean contractPresent(String orderId){
        String contractId = [SELECT ContractId FROM Order WHERE Id =: orderId].ContractId;
        return contractId != NULL ? true : false;
    }
}