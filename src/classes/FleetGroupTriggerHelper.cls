/**
 * Helper class for Fleet_Group__c Trigger.
 *
 * Version              Author                  Company                 Date                    Description
 -----------------------------------------------------------------------------------------------------------------------------
    1.0                 Matt Parrella           Coastal Cloud           May-11-2021             Creating initial version to update 'counts' field on Fleet_Group__c records
 */

public with sharing class FleetGroupTriggerHelper {

    /**
     * @description This method will contain all custom logic for the 'After Update' context
     *
     * @param newRecords - List of records contained within Trigger.new
     * @param oldMap - Map (<Id, Fleet_Group__c>) of records contained within Trigger.oldMap
     *
     * @return void
    */
    public static void handleAfterUpdate(List<Fleet_Group__c> newRecords, Map<Id,Fleet_Group__c> oldMap){
        List<Fleet_Group__c> recordsToProcess = new List<Fleet_Group__c>();
        for (Fleet_Group__c temp : newRecords){
            if (temp.Recalculate__c && (temp.Recalculate__c != oldMap.get(temp.Id).Recalculate__c)){
                recordsToProcess.add(temp);
            }
        }
        countSubscribedTails(recordsToProcess);
    }

    /**
     *  @description  This method will look at all the related System_Tail__c records and will update the following fields:
     *                          i.)     APG_Subscribed_Tails__c
     *                          ii.)    RR_Subscribed_Tails__c
     *                          iii.)   Seattle_Avionics_Subscribed_Tails__c
     *                          iv.)    Count__c
     *                 The values for fields i.-iii. will be determined by the value within the 'Name' field (example: APG-232-GC, would count towards the 'APG_Subscribed_Tails__c' tally)
     *
     * @param fleetGroupRecords - List of Fleet_Group__c records that have been triggered for recalculation (Recalculate__c has changed to TRUE)
     *
     * @return void
     */
    private static void countSubscribedTails(List<Fleet_Group__c> fleetGroupRecords){
        List<Fleet_Group__c> recordsToUpdate = new List<Fleet_Group__c>();
        System.debug('*** Called countSubscribedTails with: ' + fleetGroupRecords.size() + ' records!');
        /* 'sortedMap' structure:
            Fleet_Group__c Record Id -> 'APG' -> List of System_Tail__c records that have 'APG' in the Name
                                     -> 'RocketRoute' -> List of System_Tail__c records that have 'RocketRoute' in the Name
                                     -> 'Seattle Avionics' -> List of System_Tail__c records that have 'Seattle Avionics' in the Name
        */
        Map<Id,Map<String,List<System_Tail__c>>> sortedMap = new Map<Id,Map<String,List<System_Tail__c>>>();
        Map<Id, Set<Id>> uniqueCountMap = new Map<Id,Set<Id>>(); // Map to hold UniqueCount of Tails per Fleet Group record

        // Sort all the related System Tail records into the 'sortedMap' structure: START
        List<System_Tail__c> relatedTails = [SELECT Id, Name, Fleet_Group__c, Fleet_Group__r.Name, Tail__c FROM System_Tail__c WHERE Fleet_Group__c IN :fleetGroupRecords];
        for (System_Tail__c tempTail : relatedTails) {
            System.debug('Related to: ' + tempTail.Fleet_Group__r.Name);
            String companyName = tempTail.Name.split('-')[0]?.trim().toLowerCase(); // Extract the first portion of the Name (the Company name)
            switch on companyName {
                when 'apg' {
                    // Check if Fleet_Group__c Record ID is within the map already
                    if (sortedMap.containsKey((tempTail.Fleet_Group__c))) {
                        if (sortedMap.get(tempTail.Fleet_Group__c).containsKey('APG')) {
                            sortedMap.get(tempTail.Fleet_Group__c).get('APG').add(tempTail);
                        } else {
                            sortedMap.get(tempTail.Fleet_Group__c).put('APG', new List<System_Tail__c>{
                                    tempTail
                            });
                        }
                    } else {
                        sortedMap.put(tempTail.Fleet_Group__c, new Map<String, List<System_Tail__c>>{
                                'APG' => new List<System_Tail__c>{
                                        tempTail
                                }
                        });
                    }
                    // check the same for the uniqueCountMap
                    if (uniqueCountMap.containsKey(tempTail.Fleet_Group__c)) {
                        uniqueCountMap.get(tempTail.Fleet_Group__c).add(tempTail.Tail__c); // Adding the Tail__c value to get unique count
                    } else {
                        uniqueCountMap.put(tempTail.Fleet_Group__c, new Set<Id>{
                                tempTail.Tail__c
                        });
                    }
                }
                when 'rocketroute' {
                    // Check if Fleet_Group__c Record ID is within the map already
                    if (sortedMap.containsKey((tempTail.Fleet_Group__c))) {
                        if (sortedMap.get(tempTail.Fleet_Group__c).containsKey('RocketRoute')) {
                            sortedMap.get(tempTail.Fleet_Group__c).get('RocketRoute').add(tempTail);
                        } else {
                            sortedMap.get(tempTail.Fleet_Group__c).put('RocketRoute', new List<System_Tail__c>{
                                    tempTail
                            });
                        }
                    } else {
                        sortedMap.put(tempTail.Fleet_Group__c, new Map<String, List<System_Tail__c>>{
                                'RocketRoute' => new List<System_Tail__c>{
                                        tempTail
                                }
                        });
                    }
                    // check the same for the uniqueCountMap
                    if (uniqueCountMap.containsKey(tempTail.Fleet_Group__c)) {
                        uniqueCountMap.get(tempTail.Fleet_Group__c).add(tempTail.Tail__c); // Adding the Tail__c value to get unique count
                    } else {
                        uniqueCountMap.put(tempTail.Fleet_Group__c, new Set<Id>{
                                tempTail.Tail__c
                        });
                    }
                }
                when 'seattle avionics' {
                    // Check if Fleet_Group__c Record ID is within the map already
                    if (sortedMap.containsKey((tempTail.Fleet_Group__c))) {
                        if (sortedMap.get(tempTail.Fleet_Group__c).containsKey('Seattle Avionics')) {
                            sortedMap.get(tempTail.Fleet_Group__c).get('Seattle Avionics').add(tempTail);
                        } else {
                            sortedMap.get(tempTail.Fleet_Group__c).put('Seattle Avionics', new List<System_Tail__c>{
                                    tempTail
                            });
                        }
                    } else {
                        sortedMap.put(tempTail.Fleet_Group__c, new Map<String, List<System_Tail__c>>{
                                'Seattle Avionics' => new List<System_Tail__c>{
                                        tempTail
                                }
                        });
                    }
                    // check the same for the uniqueCountMap
                    if (uniqueCountMap.containsKey(tempTail.Fleet_Group__c)) {
                        uniqueCountMap.get(tempTail.Fleet_Group__c).add(tempTail.Tail__c); // Adding the Tail__c value to get unique count
                    } else {
                        uniqueCountMap.put(tempTail.Fleet_Group__c, new Set<Id>{
                                tempTail.Tail__c
                        });
                    }
                }
                when else {
                    // Do nothing
                    System.debug('**Hit \'else\' clause in switch statement for record: ' + tempTail);
                }
            }
            // Sort all the related System Tail records into the 'sortedMap' structure: STOP
        }// End for loop

        // Process the results
        for (Fleet_Group__c fg : fleetGroupRecords){
            Fleet_Group__c newFGRecord = new Fleet_Group__c();
            if (sortedMap.get(fg.Id) != null){
                // Set the 'APG_Subscribed_Tails__c' field value
                newFGRecord.APG_Subscribed_Tails__c = sortedMap.get(fg.Id)?.get('APG') != null ? sortedMap.get(fg.Id)?.get('APG').size() : 0;
                // Set the 'RR_Subscribed_Tails__c' field value
                newFGRecord.RR_Subscribed_Tails__c = sortedMap.get(fg.Id)?.get('RocketRoute') != null ? sortedMap.get(fg.Id)?.get('RocketRoute').size() : 0;
                // Set the 'Seattle_Avionics_Subscribed_Tails__c' field value
                newFGRecord.Seattle_Avionics_Subscribed_Tails__c = sortedMap.get(fg.Id)?.get('Seattle Avionics') != null ? sortedMap.get(fg.Id)?.get('Seattle Avionics').size() : 0;
                // Set the 'Count__c' field value
                newFGRecord.Count__c = uniqueCountMap.get(fg.Id) != null ? uniqueCountMap.get(fg.Id).size() : 0;
                newFGRecord.Id = fg.Id; // Set the Id so we can Upsert records
                newFGRecord.Recalculate__c = false; // Unset this so it can be triggered again in the future, if needed
                recordsToUpdate.add(newFGRecord);
            } else {
                // No System Tails were found, so set all counts to 0
                newFGRecord.Id = fg.Id;
                newFGRecord.APG_Subscribed_Tails__c = 0;
                newFGRecord.Seattle_Avionics_Subscribed_Tails__c = 0;
                newFGRecord.RR_Subscribed_Tails__c = 0;
                newFGRecord.Count__c = 0;
                newFGRecord.Recalculate__c = false;
                recordsToUpdate.add(newFGRecord);
            }
        }
        try {
            System.debug('!!! Updating ' + recordsToUpdate.size() + ' Fleet_Group__c records !!!');
            upsert recordsToUpdate;
        }
        Catch(DmlException e) { System.debug('****ERROR: FleetGroupTriggerHandler.countSubscribedTails() has encountered a DML fault: ' + e.getMessage()); }
    }
}