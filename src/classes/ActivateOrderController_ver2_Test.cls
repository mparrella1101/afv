@isTest
public with sharing class ActivateOrderController_ver2_Test {
    @TestSetup
    static void setupData(){

        // Get Account 'Business Account' Record Type ID
        Id BUSINESS_ACCOUNT_RTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business_Account').getRecordTypeId();

        // Get Standard Pricebook Id
        Id PRICEBOOK_ID = Test.getStandardPricebookId();


        // Create Account
        Account testAccount = new Account(
                Name = 'Apex Test Account',
                RecordTypeId = BUSINESS_ACCOUNT_RTID,
                CurrencyIsoCode = 'USD'
        );

        insert testAccount;
        // Create Opportunity record
        Opportunity testOpp = new Opportunity(
            Name = 'Apex Test Opp',
            StageName = 'Working',
            CloseDate = Date.today().addDays(7)
        );

        insert testOpp;
        // Create Account Value by Proposition record
        Account_Value_Proposition__c testProposition = new Account_Value_Proposition__c(
           Account__c = testAccount.Id,
                Name = 'test prop'
        );

        insert testProposition;

        // Create some Product records
        List<Product2> productList = new List<Product2>();
        for (Integer i = 0; i < 3; i++) {
            Product2 prod = new Product2();
            prod.Name = 'Test Product ' + i;
            prod.IsActive = TRUE;
            prod.Family = 'APG';
            if(i==1){
                prod.Family = 'RocketRoute';
            }
            if(i==2){
                prod.Family = 'Seattle Avionics';
            }
            productList.add(prod);
        }

        insert productList;

        // Create PricebookEntries
        List<PricebookEntry> PBEs = new List<PricebookEntry>();
        for (Integer i = 0; i < 3; i++){
            PBEs.add(new PricebookEntry(
               IsActive = TRUE,
                    CurrencyIsoCode = 'USD',
                    Product2Id = productList.get(i).Id,
                    Pricebook2Id = PRICEBOOK_ID,
                    UnitPrice = 100.00
            ));
        }

        insert PBEs;

        // Create some Aircraft Models
        List<ICAO_Model__c> aircraftModels = new List<ICAO_Model__c>();
        for (Integer i = 0; i < 3; i++){
            aircraftModels.add(new ICAO_Model__c(
               Model__c = 'ApexAirliner ' + i,
                    Manufacturer__c = 'Salesforce ' + i,
                    TypeDesignator__c = 'TEST'
            ));
        }
        try {
            insert aircraftModels;
        }
        Catch(DmlException e){System.debug('###: ' + e.getMessage());}
        // Create a few 'Value Proposition by Product' records
        List<Value_Proposition_by_Product__c> valuePropList = new List<Value_Proposition_by_Product__c>();
        for (Integer i = 0; i < 3; i++){
            valuePropList.add(new Value_Proposition_by_Product__c(
                    Product__c = productList.get(i).Id,
                    Aircraft__c = aircraftModels.get(i).Id,
                    Quantity__c = i + 1,
                    SubscriptionPerTailPerMonth__c = 100.00,
                    Account_Value_Proposition__c = testProposition.Id
            ));
        }
        insert valuePropList;

        // Create Contract
        Contract testContract = new Contract(
                AccountId = testAccount.Id,
                Status = 'Draft',
                StartDate = Date.today(),
                ContractTerm = 1
        );

        insert testContract;


        // Create Order record
        Order testOrder = new Order(
            AccountId = testAccount.Id,
                EffectiveDate = Date.today(),
                Contract = testContract,
                Status = 'Draft',
                CurrencyIsoCode = 'USD',
                Pricebook2Id = PRICEBOOK_ID
        );

        insert testOrder;


        // Create some OrderItem records
        List<OrderItem> orderItemList = new List<OrderItem>();
        aircraftModels = [SELECT Id, Manufacturer__c, Name FROM ICAO_Model__c WHERE Manufacturer__c LIKE 'Salesforce %'];
        productList = [SELECT Id FROM Product2 WHERE Name LIKE 'Test Product %'];
        for(Integer i = 0; i < 3; i++){
            orderItemList.add(new OrderItem(
               OrderId = testOrder.Id,
                    Aircraft__c =  aircraftModels[i].Name,
                    Product2Id = productList.get(i).Id,
                    ListPrice = 100.00,
                    UnitPrice = 100.00,
                    Quantity = 2,
                    PricebookEntryId = PBEs.get(i).Id
            ));
        }
        insert orderItemList;
      

        // Create Tail__c records
        List<Tail__c> tails = new List<Tail__c>();
        for(Integer i = 0; i < 3; i++) {
            tails.add(new Tail__c(
                    Name = 'Apex Test Tail ' + i,
                    Aircraft__c = aircraftModels[i].Id,
                    Serial_Number__c = 'APEX_' + String.valueOf(Date.today())
            ));
        }
        insert tails;

        // Create Prod System records
        List<Production_System__c> prodsystems = new List<Production_System__c>();
        for(Integer i = 0; i < 3; i++) {
            if (i == 0){
                prodsystems.add(new Production_System__c(
                        Account__c = testAccount.Id,
                        System__c = 'APG'
                ));
            }
            else if (i == 1){
                prodsystems.add(new Production_System__c(
                        Account__c = testAccount.Id,
                        System__c = 'RocketRoute'
                ));
            }
            else if (i == 2) {
                prodsystems.add(new Production_System__c(
                        Account__c = testAccount.Id,
                        System__c = 'Seattle Avionics'
                ));
            }
        }
        insert prodsystems;

        // Create the Fleet Group records
        List<Fleet_Group__c> fleetGroups = new List<Fleet_Group__c>();
        for (Integer i = 0; i < 3; i++ ){
            fleetGroups.add(new Fleet_Group__c(
                    Customer__c = testAccount.Id,
                    Aircraft__c = aircraftModels[i].Id
            ));
        }
        insert fleetGroups;

        // Create System Tail Records
        List<System_Tail__c> systemTails = new List<System_Tail__c>();
        for (Integer i = 0; i < 3; i++) {
            systemTails.add(new System_Tail__c(
                    Name = 'Apex Placeholder',
                    Tail__c = tails[i].Id,
                    Production_System__c = prodsystems[i].Id,
                    Fleet_Group__c = fleetGroups[i].Id,
                    Active__c=true
            ));
        }
        insert systemTails;

        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Product 0' LIMIT 1];
        ICAO_Model__c testAircraft = [SELECT Id FROM ICAO_Model__c WHERE Manufacturer__c = 'Salesforce 0' LIMIT 1];



        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__PriceBook__c = PRICEBOOK_ID);
        quote.SBQQ__Account__c =testAccount.Id;
        quote.SBQQ__Type__c = 'Amendment';
        quote.SBQQ__Primary__c = true;
        quote.SBQQ__SubscriptionTerm__c = 12.0;
        quote.SBQQ__StartDate__c = Date.today();
        quote.SBQQ__EndDate__c = Date.today().addDays(365);
        quote.SBQQ__Opportunity2__c = testOpp.Id;
        quote.CurrencyIsoCode = 'USD';
        quote.SBQQ__MasterContract__c = testContract.Id;
        insert quote;

        //First Record
        SBQQ__QuoteLine__c insertQuoteLine = new SBQQ__QuoteLine__c();
        insertQuoteLine.SBQQ__PricebookEntryId__c =   PBEs[0].Id;
        insertQuoteLine.SBQQ__Quote__c = quote.Id;
        insertQuoteLine.Aircraft__c =  aircraftModels[0].Id;
        insertQuoteLine.System_Tail__c = systemTails[0].Id;
        insertQuoteLine.Tail__c = tails[0].Id; 
        insertQuoteLine.SBQQ__Product__c =productList[0].Id;
        insertQuoteLine.SBQQ__Quantity__c = 1.0;
        insertQuoteLine.SBQQ__ListPrice__c = 100.00;
        insert insertQuoteLine;

        
                // Create Subscription record for old order item
                SBQQ__Subscription__c newSub = new SBQQ__Subscription__c(
                    SBQQ__Account__c = testAccount.Id,
                    SBQQ__Product__c = testProduct.Id,
                    SBQQ__Contract__c = testContract.Id,
                    Aircraft__c = testAircraft.Id,
                    SBQQ__Quantity__c = 1.0,
                    SBQQ__QuoteLine__c = insertQuoteLine.Id
            );
            insert newSub;
            // Aircraft_Registration__c airCraftReg = new Aircraft_Registration__c();
            // airCraftReg.Subscription__c = newSub.Id;
            // airCraftReg.System_Tail__c =systemTails[0].Id;
            // insert airCraftReg;
            //update Contract with Quote Id
           // testContract.SBQQ__Quote__c = quote.Id;
           // update testContract;
        //Second Record
        // SBQQ__QuoteLine__c insertQuoteLine2 = new SBQQ__QuoteLine__c();
        // insertQuoteLine2.SBQQ__PricebookEntryId__c =   PBEs[1].Id;
        // insertQuoteLine2.SBQQ__Quote__c = quote.Id;
        // insertQuoteLine2.Aircraft__c =  aircraftModels[1].Id;
        // insertQuoteLine2.System_Tail__c = systemTails[1].Id;
        // insertQuoteLine2.Tail__c = tails[1].Id; 
        // insertQuoteLine2.SBQQ__Product__c =productList[1].Id;
        // insertQuoteLine2.SBQQ__Quantity__c = 1.0;
        // insertQuoteLine2.SBQQ__ListPrice__c = 100.00;
        // insert insertQuoteLine2;
        // //Third Record
        // SBQQ__QuoteLine__c insertQuoteLine3 = new SBQQ__QuoteLine__c();
        // insertQuoteLine3.SBQQ__PricebookEntryId__c =   PBEs[2].Id;
        // insertQuoteLine3.SBQQ__Quote__c = quote.Id;
        // insertQuoteLine3.Aircraft__c =  testAircraft.Id;
        // insertQuoteLine3.System_Tail__c = systemTails[2].Id;
        // insertQuoteLine3.Tail__c = tails[2].Id; 
        // insertQuoteLine3.SBQQ__Product__c =testProduct.Id;
        // insertQuoteLine3.SBQQ__Quantity__c = 1.0;
        // insertQuoteLine3.SBQQ__ListPrice__c = 100.00;
        // insert insertQuoteLine3;
    }
    @isTest 
    public static void  TestActivateOrderController() {
        String QuoteId = '';
       // SBQQ__Quote__c theQuote = [Select Id,SBQQ__Account__c From SBQQ__Quote__c where Name = 'Q-32143' limit 1];
       SBQQ__Quote__c theQuote = [Select Id,SBQQ__Account__c From SBQQ__Quote__c limit 1];
       SBQQ__QuoteLine__c quoteLine = [Select Id,Aircraft__c,Tail__c,SBQQ__Product__c,System_Tail__c From SBQQ__QuoteLine__c  where SBQQ__Quote__c = :theQuote.Id limit 1];
       SBQQ__Subscription__c subs = [Select Id From SBQQ__Subscription__c where SBQQ__QuoteLine__c =:quoteLine.Id limit 1];



        QuoteId = String.valueOf(theQuote.Id);
        ActivateOrderController_ver2.processQuoteLines(QuoteId);
        ActivateOrderController_ver2.approachTwo(QuoteId);
        ActivateOrderController_ver2.activateContractAcceptQuote(QuoteId);
        ActivateOrderController_ver2.markOppAsWon(QuoteId);
        ActivateOrderController_ver2.processAmendmentSubs(QuoteId);

         List<ActivateOrderController_ver2.QuoteLineUpdateWrapper> quoteLineWrapList = new   List<ActivateOrderController_ver2.QuoteLineUpdateWrapper>();
         ActivateOrderController_ver2.QuoteLineUpdateWrapper quoteLineWrap = new   ActivateOrderController_ver2.QuoteLineUpdateWrapper();
        
         quoteLineWrap.subscriptionId = subs.Id;
         quoteLineWrap.sysTailId = quoteLine.Tail__c;
         quoteLineWrap.productId = quoteLine.SBQQ__Product__c;
         quoteLineWrap.productFamily = 'APG';
         quoteLineWrap.aircraftId = quoteLine.Aircraft__c;
         quoteLineWrapList.add(quoteLineWrap);
         String thejson = json.serialize(quoteLineWrapList);
       
         ActivateOrderController_ver2.deregisterSysTails(thejson,QuoteId);
        ActivateOrderController_ver2.createAircraftSubscriptions(thejson,QuoteId);         
    }
  
    @isTest 
    public static void  TestActivateOrderController2() {
        String QuoteId = '';
       // SBQQ__Quote__c theQuote = [Select Id,SBQQ__Account__c From SBQQ__Quote__c where Name = 'Q-32143' limit 1];
       SBQQ__Quote__c theQuote = [Select Id,SBQQ__Account__c From SBQQ__Quote__c limit 1];
       SBQQ__QuoteLine__c quoteLine = [Select Id,Aircraft__c,Tail__c,SBQQ__Product__c,System_Tail__c,SBQQ__PricebookEntryId__c From SBQQ__QuoteLine__c  where SBQQ__Quote__c = :theQuote.Id limit 1];
       SBQQ__Subscription__c subs = [Select Id From SBQQ__Subscription__c where SBQQ__QuoteLine__c =:quoteLine.Id limit 1];
            Aircraft_Registration__c airCraftReg = new Aircraft_Registration__c();
            airCraftReg.Subscription__c = subs.Id;
            airCraftReg.System_Tail__c =quoteLine.System_Tail__c;
            insert airCraftReg;
            //  Product2 prod = [Select Id From Product2 where Family = 'RocketRoute'];
            // // quoteLine.SBQQ__Product__c = prod.Id;
            // // update quoteLine;

            // SBQQ__QuoteLine__c insertQuoteLine2 = new SBQQ__QuoteLine__c();
            // insertQuoteLine2.SBQQ__PricebookEntryId__c =   quoteLine.SBQQ__PricebookEntryId__c;
            // insertQuoteLine2.SBQQ__Quote__c = theQuote.Id;
            // insertQuoteLine2.Aircraft__c =   quoteLine.Aircraft__c;
            // insertQuoteLine2.System_Tail__c =  quoteLine.System_Tail__c;
            // insertQuoteLine2.Tail__c =  quoteLine.Tail__c;
            // insertQuoteLine2.SBQQ__Product__c = prod.Id;
            // insertQuoteLine2.SBQQ__Quantity__c = 1.0;
            // insertQuoteLine2.SBQQ__ListPrice__c = 100.00;
            // insert insertQuoteLine2;

        QuoteId = String.valueOf(theQuote.Id);
        ActivateOrderController_ver2.processQuoteLines(QuoteId);
        ActivateOrderController_ver2.approachTwo(QuoteId);
        ActivateOrderController_ver2.activateContractAcceptQuote(QuoteId);
        ActivateOrderController_ver2.markOppAsWon(QuoteId);
        ActivateOrderController_ver2.processAmendmentSubs(QuoteId);

         List<ActivateOrderController_ver2.QuoteLineUpdateWrapper> quoteLineWrapList = new   List<ActivateOrderController_ver2.QuoteLineUpdateWrapper>();
         ActivateOrderController_ver2.QuoteLineUpdateWrapper quoteLineWrap = new   ActivateOrderController_ver2.QuoteLineUpdateWrapper();
        
         quoteLineWrap.subscriptionId = subs.Id;
         quoteLineWrap.sysTailId = quoteLine.Tail__c;
         quoteLineWrap.productId = quoteLine.SBQQ__Product__c;
         quoteLineWrap.productFamily = 'APG';
         quoteLineWrap.aircraftId = quoteLine.Aircraft__c;
         quoteLineWrapList.add(quoteLineWrap);
         String thejson = json.serialize(quoteLineWrapList);
       
         ActivateOrderController_ver2.deregisterSysTails(thejson,QuoteId);
        ActivateOrderController_ver2.createAircraftSubscriptions(thejson,QuoteId);        
        
        
        //Increase coverage for aura fields
        ActivateOrderController_ver2.SystemTailWrapper tailWrap = new  ActivateOrderController_ver2.SystemTailWrapper();
        tailWrap.value = '';
        tailWrap.label = '';
        tailWrap.productFamily = '';
        tailWrap.productId = '';
        tailWrap.registeredQuoteLineId = '';
        tailWrap.isRegistered = true;
        tailWrap.aircraftId = '';

    }
  
   




}