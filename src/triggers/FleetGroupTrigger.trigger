/**
 * Trigger for Fleet_Group__c Custom Object.
 *
 * Version              Author              Company             Date                Description
 -----------------------------------------------------------------------------------------------------------------------
    1.0                 Matt Parrella       Coastal Cloud       May-11-2021         Creating initial version to update count fields on Fleet_Group__c records
 */

trigger FleetGroupTrigger on Fleet_Group__c (after update) {

    if (Trigger.isUpdate){
        if (Trigger.isAfter){
            FleetGroupTriggerHelper.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }

}