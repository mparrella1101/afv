/**
 * The Apex Trigger for the System_Tail__c Custom Object.
 *
 *
 * Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           May-14-2021     Creating initial version in order to keep Fleet_Group__c count fields up-to-date in real-time
 */

trigger SystemTailTrigger on System_Tail__c (after update, after delete) {
    if (Trigger.isUpdate) {
        if (Trigger.isAfter) {
            SystemTailTriggerHelper.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }

    if (Trigger.isDelete){
        if (Trigger.isAfter){
            SystemTailTriggerHelper.handleAfterDelete(Trigger.old);
        }
    }
}