/**
 *
 *  Original - Coastal Cloud - Matt Parrela - 03/18/2021 - Creating initial version of Lead Trigger
 *
 */

trigger LeadTrigger on Lead (after insert, after update) {
    if (Trigger.isInsert){
        if (Trigger.isAfter){
            LeadTriggerHandler.handleAfterInsert(Trigger.new);
        }
    }

    if (Trigger.isUpdate){
        if (Trigger.isAfter){
            LeadTriggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}