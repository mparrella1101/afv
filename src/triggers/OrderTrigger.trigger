/**
 * Trigger for Order object
 *
 *
 * 02/11/2021 - Coastal Cloud - Matt Parrella - Creating Initial Version
 *
*/

trigger OrderTrigger on Order (after update) {
    if (Trigger.isUpdate){
        if (Trigger.isAfter){
            OrderTriggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}